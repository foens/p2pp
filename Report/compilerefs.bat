@ECHO OFF
REM move report.pdf r.pdf
IF ERRORLEVEL 1 (
  echo msgbox"PDF in use. Fix plz!">msgbox.vbs&msgbox.vbs
  del msgbox.vbs
) else (
  del r.pdf
  pdflatex report.tex
  bibtex report.aux
  pdflatex report.tex
  pdflatex report.tex
)