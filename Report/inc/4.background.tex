In this section we will first describe the problem we are trying to solve. Then we will define common BitTorrent terms and protocol. Finally we describe the Local Peer Discovery BitTorrent extension.


\subsection{Current problem}
To appreciate our work it's imperative to understand the problem it solves. This is illustrated with two
use cases.

\subsubsection{Software updates in enterprise environments}
Suppose you have a company with thousands of computers, each wanting to download a large software update to an installed program. Each computer could fetch the update from an internet update-server, but this has two problems: it's slow and it's ineffective because the same update is downloaded thousands of times on the same connection (WAN). The result is that a lot of bandwidth is wasted on downloading identical files - bandwidth that could potentially have been used for other more productive things in the company. To circumvent this, one could use a P2P system to both minimize internet usage and collectively download the software update and distribute it locally using the higher speed local area network. 

\subsubsection{Game updates at LAN parties}
\label{sec:usecase_lanparty}
The ultimate goal of a LAN party \cite{_lan_2013} is to provide a low latency environment for (mostly) playing games. Though players may compete against each other internally it is not uncommon to play with people outside the local network. In addition more of today's games require some form of connectivity to the Internet even to be played locally \cite{_always-drm_2013}. This poses strict requirements for the shared WAN connection; it must provide low latency and be available. Due to the nature of LAN parties this is no easy task. At any point in time game developers may release a patch requiring all computers running that game to update. This causes a huge surge in WAN traffic as hundreds of PC's will be requesting the same file from a (usually single) source on the internet. This is very bad for latency and availability and may in the worst case prevent people from even starting their games or be thrown out of servers due to high ping times. 

To solve this problem a P2P system could be utilized for downloading game updates and other large files. Blizzard is an example of a company that already utilizes P2P technology to distribute game updates \cite{_blizzard_2013}. The goal, however, is not to minimize WAN and maximize LAN traffic but instead to decrease load on the patch servers and increase the speed of the client downloads. 

What is needed to support this use case is a P2P system that would allow LAN clients to cooperate around downloading the files in such a way that it would maximize the usage of the LAN links while minimizing the use of the WAN link. In the next section we will take a look at the BitTorrent P2P system and describe how it works.

\subsection{BitTorrent}
In this section we will describe how BitTorrent works. There are a number of central terms which are:

\begin{itemize}
	\item
	A \textbf{torrent file} is an encoded dictionary containing various pieces of metadata used to bootstrap a BitTorrent clients download process and is typically very small compared to the data it describes. Torrent files are commonly found using out-of-band methods like websites. Contents include: hashcode of the complete torrent data, how many pieces exists and hashes for them, piece size, tracker urls and filenames. Notice that the torrent file does not contain any actual data, it only contains metadata. In this paper the term \textbf{torrent} will refer to the actual contents described by the \textbf{torrent file}.
	
	\item
	A \textbf{tracker} is a bootstrapping service used for finding peers in the swarm collaborating on distributing a single torrent. Clients communicate with trackers to get peers, to announce their presence or to leave the swarm.
	
	\item
	A \textbf{peer} is a single BitTorrent client instance. A peer is either a seeder or a leecher.
	
	\item
	A \textbf{seeder} is a BitTorrent peer that has all data for a torrent. The sole purpose of a seeder is to make a file available for other peers. Generally a seeder would like to use all of his upload capabilities to distribute the torrent, and would therefore prefer to upload to peers with higher download rates.
	
	\item
	A \textbf{leecher} is a BitTorrent client currently downloading a torrent. A leecher also uploads pieces of the file to other leechers. A leecher is typically interested in getting the most out of his download capabilities by preferring to collaborate with high upload rate peers. See \cref{sec:choke-unchoke-algorithm} for details.
	
	\item
	The \textbf{swarm} is all the peers that are downloading or sharing a torrent. A swarm can be very large, in the number of thousands.
	
	\item
	A \textbf{peer set} for a given peer is the set of peers that the peer is connected to and is a subset of the swarm. Peer sets are generally not very large (max $\approx$100) because the overhead of maintaining too many BitTorrent connections is detrimental. If a peer is downloading multiple torrents, a separate peer set will be maintained for each of them.
\end{itemize}

A client starts by contacting the tracker(s) mentioned in the torrent file. The client receives a subset of the swarm and selects all or some of them for its peer set. The client connects to the peers in the peer set and exchanges bitfields, which are sets describing which pieces of a torrent a given peer has. Thereafter the peers will communicate with each other using the following messages:

\begin{itemize}
	\item
	An \textbf{interested} message sent from peer $\alpha$ to peer $\beta$ means that peer $\beta$ has a piece of a torrent that peer $\alpha$ does not have and would like to download. A \textbf{not-interested} message can be sent to cancel an interested message.
	
	\item
	A \textbf{choke} message can be sent from peer $\alpha$ to peer $\beta$ to disallow $\beta$ downloading from $\alpha$. Peer $\alpha$ can allow $\beta$ to download by sending $\beta$ an \textbf{unchoke} message.
	
	\item
	A \textbf{have} message contains a piece index. When a peer $\alpha$ has downloaded a piece, $\alpha$ sends a have message to everyone in its peer set. They will then know that $\alpha$ has downloaded the piece which may change their interested state.
	
	\item
	A piece is further subdivided into blocks. A \textbf{request} message is sent for a specific piece block. This tells the receiving peer to send a piece message containing the raw data for that block. Multiple request messages can be pipelined to reduce transport latency.
	
	\item
	A \textbf{piece} message containing the piece index, block index and the raw data for that block is sent in reply to a request message. This is where the actual data transfer of a torrent takes place.
	
	\item
	A \textbf{cancel} message is used to invalidate any pipelined request messages where blocks are no longer needed since they have been downloaded elsewhere.
\end{itemize}

When a local client $l$ is connected to a remote peer $r$ it maintains a state-machine for $r$. The state machine is depicted in \Cref{fig:bittorent-client-state}. A peer starts in the \textit{NOT\_INTERESTED} state. If $l$ discovers that $r$ has pieces that $l$ doesn't have, an \textit{interested} message is sent to $r$ and $l$ moves to the \textit{INTERESTED\_CHOKED} state for $r$.

In the \textit{INTERESTED CHOKED} state for $r$ a client $l$ may receive an \textit{unchoked} message from $r$, signifying that $l$ may begin downloading from $r$. Then $l$ picks a piece block to download via its piece selection algorithm (described in \cref{sec:piece-selection}), requests it from $r$ and moves to state \textit{INTERESTED\_UNCHOKED}.

When a client $l$ is in the \textit{INTERESTED\_UNCHOKED} state for $r$ it can receive either a \textit{piece} or \textit{choked} message from $r$. A \textit{choked} message will cancel all pipelined requests sent to $r$ and change $l$'s state to \textit{INTERESTED\_CHOKED}. A \textit{piece} message will either make $l$ request further pieces if $r$ has any pieces $l$ is interested in or $r$ will not have such pieces and $l$ becomes uninterested in $r$, sends a \textit{not-interested} message and moves state to the initial \textit{NOT\_INTERESTED} state.

\begin{figure*}
	\includegraphics[width=\textwidth]{figures/bittorrent-client-state.png}
	\caption{BitTorrent client state diagram for each peer in a peer's peer set.}
	\label{fig:bittorent-client-state}
\end{figure*}

\subsubsection{Choke-unchoke algorithm}
\label{sec:choke-unchoke-algorithm}
In every client a choke-unchoke algorithm is at work. The intention is to force clients to upload data if they want to download. This is called a \textit{tit-for-tat} strategy. A peer would normally only upload pieces to peers where he is allowed to download. This creates a deadlock situation. Peer $\alpha$ will not upload to peer $\beta$ unless $\beta$ uploads to $\alpha$, but $\beta$ will not upload to $\alpha$ before $\alpha$ starts uploading to $\beta$. To alleviate the situation \textit{optimistic unchokes} are added. An \textit{optimistic unchoke} unchokes a random peer in the hope that he will begin to contribute to you. \textit{Optimistic unchokes} also allows new leechers without any pieces to download and then be able to contribute those downloaded pieces later. Optimistic unchokes may also be used as way to discover faster peers to collaborate with. Choke-unchoke states are reevaluated in periods. The standard specification suggests a 10 second period for normal choke-unchokes and a 30 second period for optimistic unchokes \cite{bram_cohen_bittorrent_2008}.

\subsubsection{Piece selection}
\label{sec:piece-selection}
A central part of every BitTorrent client is the piece selection algorithm. When a client is allowed to download from a peer (i.e. it is interested and unchoked) it must choose which piece block to fetch. The standard piece selection algorithm will prioritize the rarest piece among all peers in the peer set. A client can calculate this since it knows what pieces all peers in its peer set has. 

The rarest first piece selection algorithm has two distinct advantages. First, it minimizes the likelihood of stalling the swarm. Only few peers have a rare piece (else it wouldn't be rare). If those few peers disconnect it will be impossible to obtain the piece until they (or somebody else with that piece) reconnects. This is normally not a problem, but when only a few peers have the pieces a disconnect can be critical. The second advantage is that having a rare piece makes a client more interesting for other peers (since they want the rare pieces too), and thus it increases the possibility of collaboration.

There are two situations where most BitTorrent clients will deviate from the standard algorithm. The first is when a client has just begun downloading a torrent. In this case the client will try to get a complete piece as fast as possible such that others may become interested and begin collaborating. To fetch the piece as fast as possible a client may download multiple blocks from the same piece from multiple peers at the same time. This strategy is called \textit{random first} and is only active in the specific case where a client has no pieces. The second piece selection strategy, \emph{end-game mode}, kicks in when a client is close to completing a torrent. In this case a client will request the same block(s) from multiple peers, such that a slow peer cannot delay the completion of the torrent.

\subsection{Local Peer Discovery}
\label{sec:lpd}
Local peer discovery (LPD) is an extension to BitTorrent that will allow clients to discover peers on the local network \cite{_local_2013}. LPD is implemented using UDP broadcasting to a special multicast group specified by an IP:port pair: 239.192.152.143:6771\cite{arvidn_sourceforge.net_2013,arvid_local_2013}. The message being broadcasted is defined as follows:
\\
\begin{lstlisting}
BT-SEARCH * HTTP/1.1
Host: 239.192.152.143:6771
Port: <Local BitTorrent client port>
Infohash: <Hex-encoded hash of torrent>
\end{lstlisting}

The hash of the torrent is included for the local client to determine if it is worth taking contact (replying) to the remote peer (remote peers downloading different torrents aren't relevant for example). A client can connect to the source of the broadcast by opening a connection to source's IP address (fetched from the UDP packet) and the port given inside the broadcast message (the Port field).

Peers discovered through this mechanism are usually not handled in any special way, they are added as if they were discovered using a tracker. This neglects any possible optimizations that could come from utilizing the knowledge that some peers are on LAN. An example of a well-known BitTorrent client that handles LPD peers in a slightly special way is $\mu$Torrent. It includes different rate limits for peers discovered by LPD but no other optimizations seems to be used \cite{m4573r_bittorrent_2012}.