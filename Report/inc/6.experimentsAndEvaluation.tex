In this section we present our experiments with multiple local peers downloading a torrent from one or more internet seeders (WAN peers). We have run the experiments with an unoptimized client and an optimized client. The Ttorrent base with LPD serves as the unoptimized client while the optimized client is the implementation we presented in \cref{sec:implementation}.
 
 \subsection{Simulation limits}
 Due to limited resources we have been forced to limit our experiments in several areas:

 \begin{itemize}
 	\item \emph{Number of peers}\\
 	 Our experiments were simulated on only one and the same computer (representing multiple peers) to make experiments feasible and reproducible. We wanted to run our tests with as many peers as possible. We found that our test computer (Intel 3770K $3.5$GHz overclocked to $4.4$GHz, $8$GB RAM, Samsung 840 Pro SSD harddisk) could run with approximately 16 peers without being either CPU or disk limited. Thus most tests have been run with a number of peers around 16. 
 	\item  \emph{WAN peers}\\
 	 WAN peers have been simulated by giving a selected amount of LAN peers (again running on the same PC) the defining characteristic of WAN peers; slow upload speed and not discoverable by LPD. To do this we have implemented rate limiting on peers in Ttorrent. This is a sound approach because Ttorrent has no specific code that handles LAN peers (i.e. on the same IP subnet) specially. Had we used a client that would for example always unchoke LAN peers (those on the same subnet as itself), we could not have used this approach.
 \end{itemize}

\subsection{Variables}
We have measured two key dependent variables while performing our experiments:
\begin{enumerate}
	\item The time it takes for all leechers to have downloaded the torrent. The lower the better.
	\item The total number of copies sent by all seeders calculated by $fs / su$, where $fs$ is the file size in bytes and $su$ is the total bytes uploaded by seeders. The ideal value for this metric is $1$, as it would mean the file has been sent exactly $1$ time from the seeders (no duplicate pieces have been uploaded).
\end{enumerate}

These variables reflect our goals of minimizing WAN usage (variable 2) while maximizing speed (variable 1).

\subsection{Setup}
We have performed three types of experiments with both the Ttorrent base (LPD) and our optimizing client:
\begin{itemize}
	\item
		\textbf{Experiment: Seeder upload rate}\\
		\textit{How does increasing the upload rate limits of seeders (WAN peers) impact the dependent variables?}\\
		This experiment starts 14 leechers with no initial torrent content. They will be on the same local area network. Two seeders will provide the torrent contents for the leechers to download. The following upload rates of each seeder is tested: $\{512,\allowbreak 1024,\allowbreak 2048,\allowbreak 4096\}$\SI{}{\kibi\byte/\second}.
	
	\item
	    \textbf{Experiment: Number of leechers}\\
	    \textit{How does the number of leechers impact the dependent variables?}\\
	    This experiment starts a varying amount of leechers to download from two seeders with an upload rate limit of \SI{2048}{\kibi\byte/\second}. We test with $\{2,\allowbreak 4,\allowbreak 8\}$ leechers.
	
	\item
		\textbf{Experiment: Number of seeders}\\
		\textit{How does the number of seeders impact the dependent variables?}\\
		This experiment starts eight leechers and a varying amount of seeders, all running with a upload rate limit of \SI{2048}{\kibi\byte/\second}. We test with $\{2,\allowbreak 4,\allowbreak 8\}$ seeders.
\end{itemize}

Each of the above experiments have been run with two different torrent files. One containing \SI{50}{\mebi\byte} of data and one with \SI{500}{\mebi\byte} of data. This will reveal if torrent size has any impact on the variables in each experiment, which is relevant due to the fact that torrent sizes vary a lot in the real world.

\subsection{Graphs}
For each one of the experiments two result graphs have been constructed; one for the unoptimized version (base) and one with both the unoptimized and optimized version. The former is provided solely for the reason that it's easier to read by itself, all information in the former graphs are contained in the second graphs. The graphs (\Cref{fig:wan-base-vs-opt-50MB,fig:wan-base-vs-opt-500MB,fig:ratio-leecher-base-vs-opt-50MB,fig:ratio-leecher-base-vs-opt-500MB,fig:ratio-seeder-base-vs-opt-50MB,fig:ratio-seeder-base-vs-opt-500MB,fig:wan-base-50MB,fig:wan-base-500MB,fig:ratio-leecher-base-50MB,fig:ratio-leecher-base-500MB,fig:ratio-seeder-base-50MB,fig:ratio-seeder-base-500MB}) will be analyzed in the next section, but first we will describe their overall structure.

The graphs contain two vertical axes. The first axis (solid line, left) shows the total number of data copies uploaded by seeders. The second axes (dotted line, right) shows download time. In the graphs where both experiments are shown, the black line represents the unoptimized client while the grey line represents the optimized one.

Each experiment has been run ten times to ensure consistency. The data points in our graphs are averages of the ten simulations. Error bars have been added to show variability in our test results. The error bar values have been calculated using the standard error ($stdev.s / \sqrt{\#samples}$ where $\#samples$ is $10$). Note that in most of the graphs the error bars are so small that they can't be seen.

Some of the graphs contain negative axis values. This is an anomaly caused by the fact that the data points are separated in each part of the graphs so that the lines from the different axes doesn't cross each other.

\subsection{Unoptimized BitTorrent experiments}
In this section we will present and analyze the graphs from the experiments with the unoptimized BitTorrent client.

\subsubsection{Seeder upload rate experiment}
The graphs for this experiment can be found in \Cref{fig:wan-base-50MB,fig:wan-base-500MB} on \cpageref{fig:ratio-leecher-base-50MB}. The graphs show that increasing the upload rate limit of the seeders decrease both the number of copies uploaded by the seeders and the download time. Looking closer at the copies uploaded in both graphs, it can be seen that when using two seeders the number of copies being sent is in the range $[1.4; 1.7]$. This means at least 40\% of the internet bandwidth is wasted on downloading duplicate pieces. 

The graph for \SI{50}{\mebi\byte} shows a steady decrease in both variables as the upload rate increases and thus one could suspect that the trend continues beyond the graph.

With respect to the differences in torrent size the graphs are quite similar with the primary difference being that with the \SI{500}{\mebi\byte} file the copies uploaded seems not to decrease (as) steadily when increasing upload rate.

\subsubsection{Number of leechers experiment}
The graphs for this experiment can be found in \Cref{fig:ratio-leecher-base-50MB,fig:ratio-leecher-base-500MB}. It can be seen that increasing the amount of leechers increases the number of copies and download time, but not by a huge amount. The copies sent is in the range $[1.02; 1.20]$. On both graphs there is a slight increase in copies being sent when increasing the number of leechers, and one could theorize that adding more leechers would increase the duplication even more. On the other hand a single seeder will have a limited number of peers in its peer set, limiting the amount of leechers that can simultaneously download from it. This would force the leechers that cannot fetch pieces from the seeder to download from some of the other LAN leechers, not increasing the copies sent.

\subsubsection{Varying seeders experiment}
The graphs for this experiment can be found in \Cref{fig:ratio-seeder-base-50MB,fig:ratio-seeder-base-500MB}.
It can be seen that compared to one seeder, having more seeders dramatically increases the number of copies uploaded. This is especially true with the \SI{500}{\mebi\byte} file where the number of copies uploaded goes from around 1.2 with 1 seeder to 3 with 8 seeders. We postulate the reason for this is that more seeders mean more upload slots, and thus with 8 seeders compared to 1 the 8 leechers are able to utilize more of their bandwidth. 

\subsubsection{Unoptimized experiment conclusion}
It can be concluded from these experiments that the unoptimized Ttorrent client does not try to collaborate with peers on the local area network in any way. This was expected, as Ttorrent incorporates the standard piece selection algorithm and local and internet peers are handled uniformly. This leads to the natural question: is our optimized client any better? The next section will present the results.

\input{inc/6.1problemGraphs.tex}

\subsection{Optimized BitTorrent experiments}
The results from the optimizing client can be seen plotted against the unoptimized version in \Cref{fig:wan-base-vs-opt-50MB,fig:wan-base-vs-opt-500MB,fig:ratio-leecher-base-vs-opt-50MB,fig:ratio-leecher-base-vs-opt-500MB,fig:ratio-seeder-base-vs-opt-50MB,fig:ratio-seeder-base-vs-opt-500MB} on \cpageref{fig:wan-base-vs-opt-50MB}. In the following sections we will interpret them.

\subsubsection{Seeder upload rate experiment}
The upload rate results are shown in \Cref{fig:wan-base-vs-opt-50MB,fig:wan-base-vs-opt-500MB}.  It can be seen that the optimizing client keeps the number of seeder copies very close to one. In the \SI{500}{\mebi\byte} case (\Cref{fig:wan-base-vs-opt-500MB}), the base implementation makes the seeders send $1.65$ copies at \SI{4096}{\kibi\byte/\second}, while our optimizing client stays on $1.01$ copies. That means we shaved $(1-1.01/1.65)\cdot100\approx39\%$ off the WAN usage. With regards to the download time it can be seen that our optimized client is about twice as fast in all cases. According to \Cref{fig:wan-base-vs-opt-500MB} at \SI{512}{\kibi\byte} the unoptimized client spends around 1000s downloading while out client does it in around 500s. At the next step it's 400s vs 200s and so on.

\subsubsection{Number of leechers experiment}
Looking at the graphs in  \Cref{fig:ratio-leecher-base-vs-opt-50MB,fig:ratio-leecher-base-vs-opt-500MB} we see that the optimizing client again outperforms the unoptimized one. With the optimized client there is small increase in copies being sent when more leechers are added, but it is not as dramatic as with the unoptimized client. The optimizing client also completes the download faster.

\subsubsection{Number of seeders experiment}
\Cref{fig:ratio-seeder-base-vs-opt-50MB,fig:ratio-seeder-base-vs-opt-500MB} show the results of this experiment. The optimizing client is staying very stable around 1 copies sent from seeders while the unoptimized client seems to be sky-rocketing compared to it. With regards to the download time it can be seen that the optimized client is faster.

\input{inc/6.2solutionGraphs.tex}

\subsection{Wrap-up}
\Cref{fig:download-time-all-experiments,fig:seeder-copies-all-experiments} on \cpageref{fig:download-time-all-experiments} shows statistics for all our experiments. It can be seen that our optimizing client performs better (smaller values) in both download time and seeder copies in all cases. We believe download time is reduced as a side-effect of reducing the number of duplicate pieces fetched. This is because reducing duplicate downloads means more bandwidth can be used on fetching non-duplicate pieces.

From these graphs we have calculated that our optimized client on average  reduces seeder copies uploaded by \mbox{$\approx29\%$}, meaning that we have cut off 29\% of the WAN traffic. Furthermore the optimized client reduces average download times by \mbox{$\approx34\%$}.

\begin{figure}[p]
	\includegraphics[width=\columnwidth]{figures/graph/download-time-all-experiments.png}
	\caption{Average download time for each experiment for both the unoptimized and optimized BitTorrent implementation.}
	\label{fig:download-time-all-experiments}
\end{figure}
\begin{figure}[p]
	\includegraphics[width=\columnwidth]{figures/graph/seeder-copies-all-experiments.png}
	\caption{Average seeder copies sent for each experiment for both the unoptimized an optimized BitTorrent client.}
	\label{fig:seeder-copies-all-experiments}
\end{figure}
