In this section we will describe our implemented optimizations. First we will argue for the BitTorrent library we have chosen to base our work on. Then we will describe our concrete implementation, and finally cover the GUI we have constructed.

\subsection{Choosing a BitTorrent library}
Faced with the challenge of implementing an optimized BitTorrent client we had two choices; either
implement the client from scratch or base our work on an existing library. Due to the limited time
available for the project we went with the last one.

Our choice of library was based on several parameters. First of all, we wanted the library to be written
in a programming language we are intrinsically familiar with. Secondly, the library should not be too cumbersome
to understand, use or modify. A lightweight library with a few essential features is preferable to one that implements all BitTorrent protocol extensions in existence. After having evaluated several libraries we settled on the
\emph{Ttorrent} Java BitTorrent library \cite{maxime_petazzoni_github_2013}. Ttorrent is lightweight, easy to use and relatively easy to understand and modify.

\subsection{Implementation}
\label{sec:implementation}
The two primary features of our optimized BitTorrent client are the ability to distinguish between local and internet peers, and the custom piece selection algorithm for internet peers that prioritizes pieces not found on the local network. LPD is used to discover if a peer is local or not and broadcasts are used to notify other local peers that a piece is going to be downloaded from WAN.

As mentioned we have implemented our client by modifying the Ttorrent library \cite{maxime_petazzoni_github_2013}. We've added LPD support ensuring that we can discover local peers. We've also added a flag to distinguish local peers from internet peers. Finally we've changed the piece selection algorithm to distinguish between local and internet peers. A local peer is handled using the standard BitTorrent piece selection algorithm, whereas internet peers are handled using our custom piece selection algorithm. 

When fetching a piece from an internet peer, our client broadcasts the piece index it's fetching so that other local peers will not begin downloading that same piece from WAN. The effect is that a piece is only  fetched once from the Internet and local peers will therefore not download duplicate content from WAN. This should both maximize speed (by downloading non-LAN pieces faster) and minimize WAN usage (by not downloading irrelevant duplicate pieces).

Our piece selection strategy poses an interesting question. What happens when a client wants to download a piece but all pieces are marked as already being fetched from WAN by some other peers? In that case we fall back to the standard BitTorrent piece selection algorithm. This could make a local client download duplicate pieces, but it also protects against malicious local peers. If the client chose not to download duplicate pieces from the internet peers in the case where it has nothing else to do, another local peer could pretend to have all pieces on LAN, but never allow downloading of them. That would make sure that no pieces would be downloaded from the internet and thus stalling the local swarm.

A last optimization implemented is to always unchoke local peers. In other words this removes the tit-for-tat strategy in the local swarm. A local peer should always want to help another local peer because the cost of doing so is negligible - mostly due to the enormous amount of ul/dl capacity available on the local network.

\subsection{The GUI}
Apart from the optimizations themselves we have also implemented a graphical user interface on top
of Ttorrent.
The GUI has been an intrinsic part of our development process as it makes it possible to follow a BitTorrent download process from start to finish.

\begin{figure}
	\includegraphics[width=\columnwidth]{figures/gui.jpg}
	\caption{Our custom GUI makes it possible to follow a BitTorrent download process from start to finish.}
	\label{fig:gui}
\end{figure}

The GUI can be seen in \Cref{fig:gui}. It consists of two parts; the upper part is the progress view
and the lower part is the legend. A few statistics are shown on the upper part: total number of pieces, number of pieces left, amount of connections, number of requests, the runtime and how many times the seeder(s)
have uploaded the file to the leechers (1 is ideal).

The progress view is made up of $x$ columns and $y$ rows where $x$ is the number of pieces in the
respective torrent and $y$ is the number of leechers. The progress view displays
the piece status for each piece for each leecher. This piece status is indicated by the color
of the piece blocks as can be seen on the legend. We repeat it here in case it's
difficult to distinguish the colors:

\begin{itemize}
	\item White: the piece hasn't been touched yet.
	\item Red: the piece is currently being downloaded from LAN or WAN.
	\item Black: the piece was successfully downloaded from WAN.
	\item Blue: the piece was successfully downloaded from LAN.
	\item Magenta: the piece was successfully downloaded from WAN; but another peer.
	has also downloaded this piece from WAN (duplicate)
	\item Green: the piece is currently being downloaded from WAN by \emph{another}
	local peer. In other words this peer has received a coordinate message from another peer and will
	therefore not begin downloading this piece itself (from WAN) unless it has nothing else to do.
\end{itemize}