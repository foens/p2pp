Various work has been carried out to optimize the BitTorrent protocol.
However, none target the specific scenario of both minimizing
WAN traffic and maximizing speed while being easily applicable in
all kinds of network configurations.

\emph{Local Peer Discovery} (LPD) \cite{_local_2013} is an extension to the BitTorrent protocol
that allows BitTorrent clients to discover peers on the local network.
Compared to our approach LPD does not dictate how locally discovered peers
shall be utilized. Thus pure LPD BitTorrent clients are oblivious to the fact that
LPD peers are actually on the same local network as the clients that discovered them. 
This is how e.g. MonoTorrent \cite{alan_mcgovern_monotorrent_2013} implements LPD.
In this case the only advantage of LPD is that it provides the possibility of
discovering potentially faster peers. The net effect is that LPD provides no improvements
to BitTorrent other than what is achieved by having faster peers - at least not unless
the clients do something extra besides LPD.

\emph{BitTorrent Sync} \cite{_bittorrent_2013} is a secure distributed file synchronization
technology built on top of the BitTorrent protocol and its extensions. It
is an example of a new technology that uses LPD to speed up file transfers. There is no
documentation on how exactly LPD is implemented in BitTorrent Sync so it is difficult to
compare to our approach. However, for file synchronization one could postulate
that minimizing WAN traffic is of secondary concern, if even a concern at all, compared
to maximizing speed.

Twitter's \emph{Murder} \cite{_twitter_2013} is a set of scripts used to deploy code fast 
to large data centers using BitTorrent. Data center environments often have specific
characteristics like low latency, high bandwidth and free from ISP traffic shaping. 
Murder takes advantage of this by implementing a set of optimizations that makes it more
effective in such local environments. These optimizations include sequenced deployment, decreasing timeouts,
uploading from memory, disabling unnecessary features like DHT \cite{_bittorrent_2013-1},
protocol encryption \cite{_bittorrent_2013-2} and so on.
The main difference between Murder and our implementation is the supported environments.
Murder is intended for distributing data in controlled (i.e. minimal churn, known peers etc.), large-scale intranet environments whereas our aim is to support all types of environments, in particular
those that are dynamic in nature (i.e. the LAN party use-case in \cref{sec:usecase_lanparty}). Murder's reliance on controlled environments makes it less usable for such scenarios.

While Murder is a practical solution to the problem of data distribution in enterprise environments,
there also exists research dealing with more elaborate optimizations, some of which changes the core 
BitTorrent logic. An example of this is \cite{somani_bittorrent_2012} in which two central
optimizations  are; a locality-based (IP space) piece selection strategy and reduction
of cross-site traffic when sharing data between multiple data centers. The last optimization
is achieved by copying a few instances of the data to each network outside BitTorrent 
before initiating BitTorrent transfers. For comparison we consider the goal of this
optimization equal to minimizing WAN traffic between data center networks.
Then, the primary difference to our work is that their optimization requires
a process outside of BitTorrent to initially distribute the data to each data center 
network. Our implementation requires no outside processes; all that is required is
the optimized BitTorrent client on each participating PC.

\emph{Ono} is a system that aims to reduce cross-ISP traffic with BitTorrent \cite{choffnes_taming_2008}.
The idea is to bias peer selection in such a way that peers within the
same ISP are prioritized. This also has the potential effect of increasing
speed due to locality. Content Distribution Networks (CDNs) are used to determine peer locality.
Peers redirected to similar CDN servers are likely to be close to those and thus also possibly close to each other.
Therefore, monitoring which replica servers peers are redirected to, and comparing this between peers, it can be
determined with good probability which peers are close to each other.
Our approach and Ono are only slightly related. Ono seeks to minimize cross-ISP traffic while we want
to reduce WAN traffic. These goals are wildly different, but Ono is still relevant because
it, like our approach, modifies the BitTorrent protocol to select peers based on a completely new criteria.
In the case of Ono this criteria works on the macro level (i.e. ISP subnets) while in our case
it's on the micro level of personal or enterprise local area networks (which there may be hundreds of in each 
ISP subnet).

Most of the aforementioned research focuses on optimizing BitTorrent itself. A whole other area is
the creation of new technologies for data distribution. \emph{P2P Copy} \cite{james_fast_2011} is such a technology.
P2P Copy is a protocol designed from scratch to achieve maximum speed when distributing data in data centers.
It is shown to be an order of magnitude faster at distributing content than standard BitTorrent (and Twitter's Murder) \cite[p.2]{james_fast_2011}. The system is able to achieve this increase in speed because it's built around the single use case of data distribution in data center networks. Compared to our approach P2P Copy has no overhead. With BitTorrent and any other BitTorrent-like system there will always be overhead, mainly because BitTorrent is designed to run and scale in much more dynamic environments than P2P Copy. However, like Murder, P2P Copy relies on the controlled environment in data centers whereas our solution supports broader use.