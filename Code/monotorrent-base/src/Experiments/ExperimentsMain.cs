﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using MonoTorrent.Client;
using MonoTorrent.Common;

namespace Experiments
{
    class ExperimentsMain
    {
        static void Mai2n(string[] args)
        {
            //StartExperiment("", @"C:\Users\elumineX\Desktop\torrentExperiment", 1, 0, 1, 0);
            
            var torrent = Torrent.Load(@"C:\Users\elumineX\Desktop\torrentExperiment\SharedTorrents\debian-trackerless.torrent");
            var torrentSettings = new TorrentSettings(4, 150, 0, 0);

            // Start seed1
           /* var seed1EngineSettings = new EngineSettings(@"C:\Users\elumineX\Desktop\torrentExperiment\SharedFiles", 5000);
            var seed1Engine = new ClientEngine(seed1EngineSettings);
            seed1Engine.ChangeListenEndpoint(new IPEndPoint(IPAddress.Any, 5000));
            var seed1Manager = new TorrentManager(torrent, @"C:\Users\elumineX\Desktop\torrentExperiment\SharedFiles", torrentSettings);
            seed1Engine.Register(seed1Manager);
            seed1Engine.StartAll();*/

            // Start seed2
            var seed2EngineSettings = new EngineSettings(@"C:\Users\elumineX\Desktop\torrentExperiment\SharedFiles", 5001);
            var seed2Engine = new ClientEngine(seed2EngineSettings);
            seed2Engine.ChangeListenEndpoint(new IPEndPoint(IPAddress.Any, 5001));
            var seed2Manager = new TorrentManager(torrent, @"C:\Users\elumineX\Desktop\torrentExperiment\SharedFiles", torrentSettings);
            seed2Engine.Register(seed2Manager);
            seed2Engine.StartAll();

            // Start leech
            var leechEngineSettings = new EngineSettings(@"C:\Users\elumineX\Desktop\torrentExperiment\Leech1\", 5002);
            var leechEngine = new ClientEngine(leechEngineSettings);
            leechEngine.ChangeListenEndpoint(new IPEndPoint(IPAddress.Any, 5002));
            var leechManager = new TorrentManager(torrent, @"C:\Users\elumineX\Desktop\torrentExperiment\Leech1\", torrentSettings);
            leechEngine.Register(leechManager);
            leechEngine.StartAll();

            var sb = new StringBuilder(1024);
            while (true)
            {
                sb.Remove(0, sb.Length);
                    
               // AppendFormat(sb, "Seed1 Upload Rate:     {0} kb/s", seed1Engine.TotalUploadSpeed / 1024f);
               // AppendFormat(sb, "Seed1 Total Uploaded:  {0} mb", seed1Manager.Monitor.DataBytesUploaded/(1024f*1024f));
                AppendFormat(sb, "Seed2 Upload Rate:     {0} kb/s", seed2Engine.TotalUploadSpeed / 1024f);
                AppendFormat(sb, "Seed2 Total Uploaded:  {0} mb", seed2Manager.Monitor.DataBytesUploaded / (1024f * 1024f));
                AppendFormat(sb, "Leech Download Rate:   {0} kb/s", leechEngine.TotalDownloadSpeed / 1024f);
                AppendFormat(sb, "Leech Total Download:  {0} mb",  leechManager.Monitor.DataBytesDownloaded/(1024f*1024f));
                AppendFormat(sb, "Leech Completion:      {1:0.00}% - {0}", torrent.Files[0].Path, torrent.Files[0].BitField.PercentComplete);

                Console.Clear();
                Console.WriteLine(sb.ToString());

                Thread.Sleep(100);
            }

            /*
            foreach (string file in Directory.GetFiles(torrentsPath))
            {
                if (file.EndsWith(".torrent"))
                {
                    try
                    {
                        // Load the .torrent from the file into a Torrent instance
                        // You can use this to do preprocessing should you need to
                        var torrent = Torrent.Load(file);
                        Console.WriteLine(torrent.InfoHash.ToString());
                    }
                    catch (Exception e)
                    {
                        Console.Write("Couldn't decode {0}: ", file);
                        Console.WriteLine(e.Message);
                        continue;
                    }
                    // When any preprocessing has been completed, you create a TorrentManager
                    // which you then register with the engine.
                    TorrentManager manager = new TorrentManager(torrent, downloadsPath, torrentDefaults);
                    engine.Register(manager);

                    // Store the torrent manager in our list so we can access it later
                    torrents.Add(manager);
                    manager.PeersFound += new EventHandler<PeersAddedEventArgs>(manager_PeersFound);
                }
            }*/
        }

        private static void Main2()
        {
            
        }

        private static void AppendFormat(StringBuilder sb, string str, params object[] formatting)
        {
            if (formatting != null)
                sb.AppendFormat(str, formatting);
            else
                sb.Append(str);
            sb.AppendLine();
        }

        static void StartExperiment(string sharedFile, string workingDirectory, int nrOfWanSeeders, int nrOfLanSeeders, int nrOfLanLeechers, int wanUlSpeedLimit)
        {
            if (!Directory.Exists(workingDirectory))
                Directory.CreateDirectory(workingDirectory);

            CreatePeerDirectories("WanSeed", workingDirectory, nrOfWanSeeders);
            CreatePeerDirectories("LanSeed", workingDirectory, nrOfLanSeeders);
            CreatePeerDirectories("LanLeech", workingDirectory, nrOfLanLeechers);


        }

        private static void CreatePeerDirectories(string peerBaseName, string workingDirectory, int nrOfPeers)
        {
            for (int i = 0; i < nrOfPeers; i++)
            {
                string peerName = string.Format("{0}{1}", peerBaseName, i+1);
                string peerDiretory = Path.Combine(workingDirectory, peerName);
                Directory.CreateDirectory(peerDiretory);

                string peerTorrentDir = Path.Combine(peerDiretory, "Torrents");
                Directory.CreateDirectory(peerTorrentDir);

                string peerDownloadDir = Path.Combine(peerDiretory, "Downloads");
                Directory.CreateDirectory(peerDownloadDir);
            }
        }
    }
}
