﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using MonoTorrent;
using MonoTorrent.BEncoding;
using MonoTorrent.Client;
using MonoTorrent.Client.Encryption;
using MonoTorrent.Client.Tracker;
using MonoTorrent.Common;

namespace Experiments
{
    internal class NewMain
    {
        
        
        private static string basePath;
        private static string downloadsPath;
        private static string torrentsPath;
        private static ClientEngine engine; // The engine used for downloading
        private static List<TorrentManager> torrents;
        private static Top10Listener listener;
        private static int nextFreePort = 5000;
        private static int nrOfRunningSeeders = 0;
        private static int nrOfRunningLeechers = 0;

        

        

        




        // args: workingDir, port, ulSpeedLimit
        private static void Main(string[] args)
        {
            Experiment experiment = new Experiment(@"C:\Users\elumineX\Desktop\test", 6, 1, 500,
                                                   @"C:\Users\elumineX\Desktop\test\debian-7.0.0-armhf-netinst.iso-trackerless.torrent", new Experiment.PortManager(5000));
            experiment.Run();

          //  RunExperiment(1, 1, 0, new FileStream(@"C:\Users\elumineX\Desktop\test\debian-trackerless.torrent", FileMode.Open));

            /*basePath = args[0];
            torrentsPath = Path.Combine(basePath, "Torrents");
            downloadsPath = Path.Combine(basePath, "Downloads");
            torrents = new List<TorrentManager>(); // This is where we will store the torrentmanagers
            listener = new Top10Listener(10);

            // We need to cleanup correctly when the user closes the window by using ctrl-c
            // or an unhandled exception happens
            Console.CancelKeyPress += delegate { shutdown(); };
            AppDomain.CurrentDomain.ProcessExit += delegate { shutdown(); };
            AppDomain.CurrentDomain.UnhandledException += delegate(object sender, UnhandledExceptionEventArgs e)
                {
                    Console.WriteLine(e.ExceptionObject);
                    shutdown();
                };
            Thread.GetDomain().UnhandledException += delegate(object sender, UnhandledExceptionEventArgs e)
                {
                    Console.WriteLine(e.ExceptionObject);
                    shutdown();
                };

            StartEngine(Convert.ToInt32(args[1]), Convert.ToInt32(args[2]));*/
        }

        private static void StartEngine(int port, int ulSpeedLimit)
        {
            Torrent torrent = null;
            EngineSettings engineSettings = new EngineSettings(downloadsPath, port);
            engineSettings.PreferEncryption = false;
            engineSettings.AllowedEncryption = EncryptionTypes.All;
            engineSettings.GlobalMaxUploadSpeed = ulSpeedLimit*1024;
            
            TorrentSettings torrentDefaults = new TorrentSettings(4, 150, 0, 0);

            // Create an instance of the engine.
            engine = new ClientEngine(engineSettings);
            engine.ChangeListenEndpoint(new IPEndPoint(IPAddress.Any, port));

            // If the SavePath does not exist, we want to create it.
            if (!Directory.Exists(engine.Settings.SavePath))
                Directory.CreateDirectory(engine.Settings.SavePath);

            // If the torrentsPath does not exist, we want to create it
            if (!Directory.Exists(torrentsPath))
                Directory.CreateDirectory(torrentsPath);

            // For each file in the torrents path that is a .torrent file, load it into the engine.
            foreach (string file in Directory.GetFiles(torrentsPath))
            {
                if (file.EndsWith(".torrent"))
                {
                    try
                    {
                        // Load the .torrent from the file into a Torrent instance
                        // You can use this to do preprocessing should you need to
                        torrent = Torrent.Load(file);
                        Console.WriteLine(torrent.InfoHash.ToString());
                    }
                    catch (Exception e)
                    {
                        Console.Write("Couldn't decode {0}: ", file);
                        Console.WriteLine(e.Message);
                        continue;
                    }
                    // When any preprocessing has been completed, you create a TorrentManager
                    // which you then register with the engine.
                    TorrentManager manager = new TorrentManager(torrent, downloadsPath, torrentDefaults);
                    engine.Register(manager);

                    // Store the torrent manager in our list so we can access it later
                    torrents.Add(manager);
                    manager.OnPeerFound += ManagerOnOnPeerFound;
                }
            }

            // If we loaded no torrents, just exist. The user can put files in the torrents directory and start
            // the client again
            if (torrents.Count == 0)
            {
                Console.WriteLine("No torrents found in the Torrents directory");
                Console.WriteLine("Exiting...");
                engine.Dispose();
                return;
            }

            // For each torrent manager we loaded and stored in our list, hook into the events
            // in the torrent manager and start the engine.
            foreach (TorrentManager manager in torrents)
            {
                // Every time a piece is hashed, this is fired.
                /*manager.PieceHashed += delegate(object o, PieceHashedEventArgs e)
                    {
                        lock (listener)
                            listener.WriteLine(string.Format("Piece Hashed: {0} - {1}", e.PieceIndex,
                                                             e.HashPassed ? "Pass" : "Fail"));
                    };*/

                // Every time the state changes (Stopped -> Seeding -> Downloading -> Hashing) this is fired
                manager.TorrentStateChanged += delegate(object o, TorrentStateChangedEventArgs e)
                    {
                        lock (listener)
                            listener.WriteLine("OldState: " + e.OldState.ToString() + " NewState: " +
                                               e.NewState.ToString());
                    };

                // Every time the tracker's state changes, this is fired
                foreach (TrackerTier tier in manager.TrackerManager)
                {
                    foreach (MonoTorrent.Client.Tracker.Tracker t in tier.Trackers)
                    {
                        t.AnnounceComplete += delegate(object sender, AnnounceResponseEventArgs e)
                            {
                                //listener.WriteLine(string.Format("{0}: {1}", e.Successful, e.Tracker.ToString()));
                            };
                    }
                }
                // Start the torrentmanager. The file will then hash (if required) and begin downloading/seeding
                manager.Start();
            }

            // While the torrents are still running, print out some stats to the screen.
            // Details for all the loaded torrent managers are shown.
            int i = 0;
            bool running = true;
            StringBuilder sb = new StringBuilder(1024);
            while (running)
            {
                if ((i++)%10 == 0)
                {
                    Console.Clear();
                    sb.Remove(0, sb.Length);
                    running = torrents.Exists(delegate(TorrentManager m) { return m.State != TorrentState.Stopped; });
                    AppendFormat(sb, "Open Connections:    {0}", engine.ConnectionManager.OpenConnections);

                    foreach (TorrentManager manager in torrents)
                    {
                        AppendSeperator(sb);
                        AppendFormat(sb, "Peers known:     {0}", manager.Peers.ActivePeers.Count + manager.Peers.AvailablePeers.Count + manager.Peers.BusyPeers.Count + manager.Peers.BannedPeers.Count);
                        AppendFormat(sb, "State:           {0}", manager.State);
                        AppendFormat(sb, "Name:            {0}",
                                     manager.Torrent == null ? "MetaDataMode" : manager.Torrent.Name);
                        AppendFormat(sb, "Progress:           {0:0.00}", manager.Progress);
                        AppendFormat(sb, "Download Speed:     {0:0.00} kB/s", manager.Monitor.DownloadSpeed/1024.0);
                        AppendFormat(sb, "Upload Speed:       {0:0.00} kB/s", manager.Monitor.UploadSpeed/1024.0);
                        AppendFormat(sb, "Total Downloaded:   {0:0.00} MB",
                                     manager.Monitor.DataBytesDownloaded/(1024.0*1024.0));
                        AppendFormat(sb, "Total Uploaded:     {0:0.00} MB",
                                     manager.Monitor.DataBytesUploaded/(1024.0*1024.0));
                        MonoTorrent.Client.Tracker.Tracker tracker = manager.TrackerManager.CurrentTracker;
                        if (Math.Abs(manager.Progress - 100) < 0.01 && Console.Title[0] != '#')
                            Console.Title = "##" + Console.Title;
                        //AppendFormat(sb, "Tracker Status:     {0}", tracker == null ? "<no tracker>" : tracker.State.ToString());
                        /*AppendFormat(sb, "Warning Message:    {0}",
                                     tracker == null ? "<no tracker>" : tracker.WarningMessage);
                        AppendFormat(sb, "Failure Message:    {0}",
                                     tracker == null ? "<no tracker>" : tracker.FailureMessage);
                        if (manager.PieceManager != null)
                            AppendFormat(sb, "Current Requests:   {0}", manager.PieceManager.CurrentRequestCount());
                        
                        foreach (PeerId p in manager.GetPeers())
                            AppendFormat(sb, "\t{2} - {1:0.00}/{3:0.00}kB/sec - {0}", p.Peer.ConnectionUri,
                                         p.Monitor.DownloadSpeed/1024.0,
                                         p.AmRequestingPiecesCount,
                                         p.Monitor.UploadSpeed/1024.0);

                        AppendFormat(sb, "", null);
                        if (manager.Torrent != null)
                            foreach (TorrentFile file in manager.Torrent.Files)
                                AppendFormat(sb, "{1:0.00}% - {0}", file.Path, file.BitField.PercentComplete);*/
                    }
                    
                    Console.WriteLine(sb.ToString());
                    listener.ExportTo(Console.Out);
                }

                System.Threading.Thread.Sleep(100);
            }
        }

        private static void ManagerOnOnPeerFound(object sender, PeerAddedEventArgs e)
        {
          /*  lock (listener)
                listener.WriteLine("Found new peer: " + e.Peer);*/
        }

        private static void AppendSeperator(StringBuilder sb)
        {
            AppendFormat(sb, "", null);
            AppendFormat(sb, "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -", null);
            AppendFormat(sb, "", null);
        }

        private static void AppendFormat(StringBuilder sb, string str, params object[] formatting)
        {
            if (formatting != null)
                sb.AppendFormat(str, formatting);
            else
                sb.Append(str);
            sb.AppendLine();
        }

        private static void shutdown()
        {
            BEncodedDictionary fastResume = new BEncodedDictionary();
            for (int i = 0; i < torrents.Count; i++)
            {
                torrents[i].Stop();
                ;
                while (torrents[i].State != TorrentState.Stopped)
                {
                    Console.WriteLine("{0} is {1}", torrents[i].Torrent.Name, torrents[i].State);
                    Thread.Sleep(250);
                }

                fastResume.Add(torrents[i].Torrent.InfoHash.ToHex(), torrents[i].SaveFastResume().Encode());
            }

            engine.Dispose();

            foreach (TraceListener lst in Debug.Listeners)
            {
                lst.Flush();
                lst.Close();
            }

            System.Threading.Thread.Sleep(2000);
        }
    }
}
