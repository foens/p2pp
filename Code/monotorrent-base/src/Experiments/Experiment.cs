﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MonoTorrent.Client;
using MonoTorrent.Client.Encryption;
using MonoTorrent.Common;

namespace Experiments
{
    class Experiment
    {
        public class ExperimentResults
        {
            public int NrLeechers { get; private set; }
            public int NrSeeders { get; private set; }
            public int WanSpeedKbps { get; private set; }
            public int CompletionTimeS { get; private set; }
            public long TotalBytesWanUpload { get; private set; }
            public long FileSizeBytes { get; private set; }

            public ExperimentResults(int nrLeechers, int nrSeeders, int wanSpeedKbs, int completionTimeS, long totalBytesWanUpload, long fileSizeBytes)
            {
                NrLeechers = nrLeechers;
                NrSeeders = nrSeeders;
                WanSpeedKbps = wanSpeedKbs;
                CompletionTimeS = completionTimeS;
                TotalBytesWanUpload = totalBytesWanUpload;
                FileSizeBytes = fileSizeBytes;
            }
        }

        public class PortManager
        {
            private int nextFreePort;

            public PortManager(int startPort)
            {
                nextFreePort = startPort;
            }

            public int GetNextFreePort()
            {
                return nextFreePort++;
            }
        }

        private string workingDir;
        private int nrLeechers;
        private int nrSeeders;
        private int wanSpeedKbps;
        private string torrentFile;
        private int nrOfRunningLeechers;
        private PortManager portManager;
        private List<TorrentManager> seeders = new List<TorrentManager>();
        private List<TorrentManager> leechers = new List<TorrentManager>();
  
        public Experiment(string workingDir, int nrLeechers, int nrSeeders, int wanSpeedKbps, string torrentFile, PortManager portManager)
        {
            this.workingDir = workingDir;
            this.nrLeechers = nrLeechers;
            this.nrSeeders = nrSeeders;
            this.wanSpeedKbps = wanSpeedKbps;
            this.torrentFile = torrentFile;
            this.portManager = portManager;
        }

        public ExperimentResults Run()
        {
            CheckSourceFilePresence();

            for (int i = 0; i < nrSeeders; i++)
            {
                TorrentManager manager = StartSeeder();
                seeders.Add(manager);
            }

            for (int i = 0; i < nrLeechers; i++)
            {
                TorrentManager manager = StartLeecher();
                leechers.Add(manager);
            }

            Console.WriteLine("Waiting for leecher completion...");
            WaitForCompletion();
            Console.WriteLine("All leechers finished!");

            StopAndCleanup();

            return null;
        }

        private void StopAndCleanup()
        {
            foreach (var seeder in seeders)
            {
                seeder.Stop();
                seeder.Engine.StopAll();
                seeder.Engine.Dispose();
            }
                
            foreach (var leecher in leechers)
            {
                leecher.Stop();
                leecher.Engine.StopAll();
                leecher.Engine.Dispose();
            }  
        }

        private void WaitForCompletion()
        {
            int nrLeechersCompleted = 0;

            foreach (var leecher in leechers)
            {
                leecher.TorrentStateChanged += 
                    delegate(object sender, TorrentStateChangedEventArgs args)
                    { 
                        if (args.NewState == TorrentState.Seeding)
                        {
                            nrLeechersCompleted++;
                        } 
                    };
            }

            while (nrLeechersCompleted != leechers.Count)
            {
                Console.SetCursorPosition(0, 1);
                foreach (var leecher in leechers)
                {
                    int nrKnownPeers = leecher.Peers.ActivePeers.Count + leecher.Peers.AvailablePeers.Count +
                                       leecher.Peers.BusyPeers.Count + leecher.Peers.BannedPeers.Count;

                    string connectedIps =
                        leecher.GetPeers()
                               .Select(peer => peer.Uri.Port.ToString()).DefaultIfEmpty()
                               .Aggregate((cur, next) => cur+ ", " + next);

                    Console.WriteLine("Leecher {0}: {1:N1}% [Peers: {2}, Connected: {3}]", leecher.Engine.Settings.ListenPort, leecher.Progress, nrKnownPeers, connectedIps);
                }

                double totalCompletion = leechers.Average(leecher => leecher.Progress);
                Console.Write("Total: {0:N1}% complete", totalCompletion);
                Thread.Sleep(1000);
            }
        }

        private void CheckSourceFilePresence()
        {
            Torrent torrent = Torrent.Load(torrentFile);

            if (torrent.Files.Any(file => !File.Exists(Path.Combine(workingDir, file.FullPath))))
            {
                throw new FileNotFoundException("Could not find source file in dir " + workingDir);
            }
        }

        private TorrentManager StartSeeder()
        {
            return StartClient(true);
        }

        private TorrentManager StartLeecher()
        {
            return StartClient(false);
        }

        private TorrentManager StartClient(bool seeder)
        {
            string clientDir = Path.Combine(workingDir, seeder ? "" : "Leecher" + ++nrOfRunningLeechers);
            Directory.CreateDirectory(clientDir);
            int port = portManager.GetNextFreePort();

            var engineSettings = new EngineSettings(clientDir, port)
            {
                PreferEncryption = false,
                AllowedEncryption = EncryptionTypes.All,
                GlobalMaxUploadSpeed = seeder ? 0 : wanSpeedKbps * 1024
            };

            var torrentSettings = new TorrentSettings(4, 150, 0, 0);

            var engine = new ClientEngine(engineSettings);
            engine.ChangeListenEndpoint(new IPEndPoint(IPAddress.Any, port));
       
            Torrent torrent = Torrent.Load(torrentFile);

            var manager = new TorrentManager(torrent, clientDir, torrentSettings);
            engine.Register(manager);
   
            manager.Start();

            return manager;
        }
    }
}
