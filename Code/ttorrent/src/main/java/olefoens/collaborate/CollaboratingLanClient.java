package olefoens.collaborate;

import olefoens.discovery.LocalPeerDiscoveryClient;

import java.io.IOException;
import java.net.InetAddress;

public class CollaboratingLanClient extends LocalPeerDiscoveryClient
{
	private final CollaboratingPieceSelectionTorrent torrent;

	public CollaboratingLanClient(InetAddress address, CollaboratingPieceSelectionTorrent torrent) throws IOException
	{
		super(address, torrent);
		this.torrent = torrent;
		this.torrent.giveClient(service.getSocketAddress().getPort(), this);
	}

	@Override
	public void stop(boolean wait)
	{
		super.stop(wait);
		torrent.stop();
	}
}