package olefoens.collaborate;

import com.turn.ttorrent.client.Client;
import com.turn.ttorrent.client.Piece;
import com.turn.ttorrent.client.SharedTorrent;
import com.turn.ttorrent.client.peer.SharingPeer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.*;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CollaboratingPieceSelectionTorrent extends SharedTorrent implements Runnable
{
	private final Pattern pattern = Pattern.compile("BT-COLLABORATE.*Port: (?<port>\\d+)\r\nPieces: (?<pieces>.*)\r\nInfohash: (?<hash>.*)\r\n\r\n\r\n", Pattern.DOTALL | Pattern.MULTILINE);
	public BitSet collaboratorsFetchingPieces = new BitSet();
	private Thread thread;

	public CollaboratingPieceSelectionTorrent(byte[] data, File parent, int uploadRateInKilobytes) throws IOException, NoSuchAlgorithmException
	{
		super(data, parent, uploadRateInKilobytes);

		try
		{
			this.socket = new DatagramSocket();

			receiveSocket = new MulticastSocket(BROADCAST_PORT);
			receiveSocket.joinGroup(InetAddress.getByName(BROADCAST_GROUP));
			receiveSocket.setLoopbackMode(false);
			receiveSocket.setSoTimeout(10000);
		} catch (SocketException e)
		{
			throw new RuntimeException(e);
		} catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public synchronized void handlePeerReady(SharingPeer peer)
	{
		if(peer.isLocalPeer)
		{
			super.handlePeerReady(peer);
			return;
		}

		BitSet interesting = peer.getAvailablePieces();
		interesting.andNot(completedPieces);
		interesting.andNot(requestedPieces);
		interesting.andNot(collaboratorsFetchingPieces);

		if (interesting.cardinality() == 0)
		{
			super.handlePeerReady(peer);
			Piece piece = peer.getRequestedPiece();
			if(piece != null)
				broadcast(createChosenBitSet(piece.getIndex()));
			//peer.unbind(false);
			return;
		}

		ArrayList<Piece> choice = new ArrayList<Piece>(RAREST_PIECE_JITTER);
		synchronized (this.rarest)
		{
			for (Piece piece : this.rarest)
			{
				if (interesting.get(piece.getIndex()))
				{
					choice.add(piece);
					if (choice.size() >= RAREST_PIECE_JITTER)
					{
						break;
					}
				}
			}
		}

		Piece chosen = choice.get(
				this.random.nextInt(
						Math.min(choice.size(),
								RAREST_PIECE_JITTER)));
		this.requestedPieces.set(chosen.getIndex());

		logger.trace("Requesting {} from {}, we now have {} " +
				"outstanding request(s): {}",
				new Object[]{
						chosen,
						peer,
						this.requestedPieces.cardinality(),
						this.requestedPieces
				});

		peer.downloadPiece(chosen);
		broadcast(createChosenBitSet(chosen.getIndex()));
	}

	private BitSet createChosenBitSet(int pieceIndex)
	{
		BitSet bitSet = new BitSet();
		bitSet.set(pieceIndex);
		return bitSet;
	}

	public synchronized void handleCollaboratorFetchingPieces(BitSet bitSet)
	{
		collaboratorsFetchingPieces.or(bitSet);
	}

	private static final Logger logger = LoggerFactory.getLogger(CollaboratingPieceSelectionTorrent.class);

	private int port;
	private Client client;
	private DatagramSocket socket;
	private static final int BROADCAST_PORT = 6771;
	private MulticastSocket receiveSocket;
	private static final String BROADCAST_GROUP = "239.192.152.143";

	private volatile boolean keepRunning = true;


	public void giveClient(int port, Client client)
	{
		this.port = port;
		this.client = client;

		keepRunning = true;
		thread = new Thread(this);
		thread.start();
	}

	public static CollaboratingPieceSelectionTorrent fromFile(File source, File parent, int uploadRateInKilobytes)
			throws IOException, NoSuchAlgorithmException {
		FileInputStream fis = new FileInputStream(source);
		byte[] data = new byte[(int)source.length()];
		fis.read(data);
		fis.close();
		return new CollaboratingPieceSelectionTorrent(data, parent, uploadRateInKilobytes);
	}

	public void stop()
	{
		keepRunning = false;
		thread.interrupt();
	}

	private void broadcast(BitSet bitSet)
	{
		byte[] message = generateBroadcastMessage(bitSet);
		try
		{
			logger.info("Sending broadcast message");
			socket.send(new DatagramPacket(message, message.length, InetAddress.getByName(BROADCAST_GROUP), BROADCAST_PORT));
		} catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	private byte[] generateBroadcastMessage(BitSet bitSet)
	{
		String message = "BT-COLLABORATE * HTTP/1.1\r\n" +
				"Host: 239.192.152.143:6771\r\n" +
				"Port: " + port + "\r\n" +
				"Pieces: " + bitSet.toString() + "\r\n" +
				"Infohash: " + getHexInfoHash() + "\r\n" +
				"\r\n\r\n";
		logger.debug("Sending broadcast message: {}", message);
		return message.getBytes();
	}

	public BitSet parse(String bitsetString)
	{
		BitSet bs = new BitSet();
		String[] tokens = bitsetString.replaceAll("\\{", "").replaceAll("}", "").split(",");
		int i;
		if (tokens.length > 0)
		{
			for (String s : tokens)
			{
				s = s.trim();
				if (!s.isEmpty())
				{
					i = Integer.parseInt(s);
					bs.set(i);
				} else
				{
					break;
				}
			}
		}
		return bs;
	}

	private BroadcastInfo parseBroadcastMessage(DatagramPacket packet) throws BadBroadcastMessage
	{
		String message = new String(packet.getData(), 0, packet.getLength());
		logger.debug("Received broadcast message: {}", message);
		Matcher matcher = pattern.matcher(message);
		if (matcher.matches())
		{
			int port = Integer.parseInt(matcher.group("port"));
			String infoHash = matcher.group("hash");
			logger.debug("Broadcast message with port {} and hash {}", port, infoHash);

			BitSet bitset = parse(matcher.group("pieces"));

			return new BroadcastInfo(packet.getAddress().getHostAddress(), port, bitset);
		} else
		{
			throw new BadBroadcastMessage();
		}
	}

	private void handleReceivedBroadcasts() throws IOException
	{
		try
		{
			byte[] buffer = new byte[1024];
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			receiveSocket.receive(packet);

			BroadcastInfo broadcastInfo = parseBroadcastMessage(packet);

			if (client.getPeerSpec().getAddress().equals(packet.getAddress()) && client.getPeerSpec().getPort() == broadcastInfo.port)
			{
				// Ignore own packets
				return;
			}

			handleCollaboratorFetchingPieces(broadcastInfo.bitSet);

		} catch (SocketTimeoutException e)
		{
			// No data received. Just ignore
		} catch (BadBroadcastMessage badBroadcastMessage)
		{
			// Don't do anything
			logger.error("Invalid broadcast message");
		}
	}

	public void run()
	{
		try
		{
			while (keepRunning)
			{
				handleReceivedBroadcasts();
			}
		} catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	private class BroadcastInfo
	{
		public String ip;
		public int port;
		public BitSet bitSet;

		public BroadcastInfo(String ip, int port, BitSet bitSet)
		{
			this.ip = ip;
			this.port = port;
			this.bitSet = bitSet;
		}
	}

	private class BadBroadcastMessage extends Exception
	{
	}
}
