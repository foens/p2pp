package olefoens.tests;

import java.io.File;

public class TestSetup
{
	public final int seeders;
	public final int leechers;
	public final int seederUploadRateInKilobyte;
	public final File torrent;
	public final ClientType clientType;
	public final int testRuns;

	public TestSetup(int seeders, int leechers, int seederUploadRateInKilobyte, File torrent, ClientType clientType, int testRuns)
	{
		this.seeders = seeders;
		this.leechers = leechers;
		this.seederUploadRateInKilobyte = seederUploadRateInKilobyte;
		this.torrent = torrent;
		this.clientType = clientType;
		this.testRuns = testRuns;
	}
}
