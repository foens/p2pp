package olefoens.tests;

class TestResult
{
	public final TestSetup setup;
	public final double copies;
	public final long size;
	public final long totalDownload;
	public final long totalUpload;
	public final long downloadTimeInMs;

	TestResult(TestSetup setup, double copies, long size, long totalDownload, long totalUpload, long downloadTimeInMs)
	{
		this.setup = setup;
		this.copies = copies;
		this.size = size;
		this.totalDownload = totalDownload;
		this.totalUpload = totalUpload;
		this.downloadTimeInMs = downloadTimeInMs;
	}
}
