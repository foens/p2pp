package olefoens.tests;

import com.turn.ttorrent.client.Client;
import com.turn.ttorrent.client.SharedTorrent;
import com.turn.ttorrent.client.peer.SharingPeer;
import com.turn.ttorrent.common.Peer;
import olefoens.collaborate.CollaboratingLanClient;
import olefoens.collaborate.CollaboratingPieceSelectionTorrent;
import olefoens.discovery.LocalPeerDiscoveryClient;
import olefoens.giveaway.LanGiveAwayClient;

import javax.swing.*;
import java.io.File;
import java.net.InetAddress;
import java.util.ArrayList;

public class TestRunner
{
	private final File outputDirectory;
	private final boolean showGui;
	private PieceCompletionGui testGui;
	private JFrame frame;
	private static final int leecherShareTimeInSeconds = 10;

	public TestRunner(File outputDirectory, boolean showGui)
	{
		this.outputDirectory = outputDirectory;
		this.showGui = showGui;
	}

	public void runTests(ArrayList<TestSuite> suites, TestResultPrinter printer)
	{
		for (TestSuite testSuite : suites)
		{
			String testName = testSuite.name;
			printer.openTestRun(testName);
			for (TestSetup setup : testSuite.setups)
			{
				for (int tests = 0; tests < setup.testRuns; tests++)
				{
					ArrayList<Client> seeders = new ArrayList<Client>();
					ArrayList<Client> leechers = new ArrayList<Client>();

					for (int i = 0; i < setup.leechers; i++)
					{
						System.out.println("Leecher " + i);
						leechers.add(createLeecher(i, setup.clientType, setup.torrent, seeders));
					}

					for (int i = 0; i < leechers.size(); i++)
					{
						Client leecher = leechers.get(i);
						System.out.println("Leecher start " + i);


						leecher.share(leecherShareTimeInSeconds);
						while (!leecher.service.isAlive())
						{
							try
							{
								Thread.sleep(1);
							} catch (InterruptedException e)
							{
								e.printStackTrace();
							}
						}
					}

					showGui(leechers, testName, setup);

					try
					{
						Thread.sleep(1000);
					} catch (InterruptedException e)
					{
						e.printStackTrace();
					}

					for (int i = 0; i < setup.seeders; i++)
					{
						System.out.println("Seeder " + i);
						Client seeder = startSeeder(setup.seederUploadRateInKilobyte, setup.torrent);
						seeders.add(seeder);

						while (!seeder.service.isAlive())
						{
							try
							{
								Thread.sleep(1);
							} catch (InterruptedException e)
							{
								e.printStackTrace();
							}
						}
					}

					long leechStart = System.currentTimeMillis();

					giveGuiStartTime(leechStart);

					for (int i = 0; i < leechers.size(); i++)
					{
						Client leecher = leechers.get(i);
						ArrayList<Peer> peers = new ArrayList<Peer>();

						for (Client leecher2 : leechers)
						{
							if (leecher != leecher2)
							{
								Peer peerSpec = leecher.getPeerSpec();
								Peer peer = new Peer(peerSpec.getIp(), peerSpec.getPort());
								peer.isLocalPeer = true;
								peers.add(peer);
							}
						}

						for (Client seeder : seeders)
							peers.add(seeder.getPeerSpec());

						while (!leecher.service.isAlive())
						{
							try
							{
								Thread.sleep(1);
							} catch (InterruptedException e)
							{
								e.printStackTrace();
							}
						}

						for(SharingPeer p : leecher.getPeers())
						{
							p.isLocalPeer = true;
						}

						leecher.handleDiscoveredPeers(peers);
					}

					for (Client leecher : leechers)
					{
						leecher.waitForCompletion();
					}

					long leechEnd = System.currentTimeMillis() - leecherShareTimeInSeconds * 1000;

					for (Client seeder : seeders)
					{
						seeder.stop(true);
					}

					long seederTotalUpload = 0;
					long size = 47710208;
					for (Client seeder : seeders)
					{
						seederTotalUpload += seeder.getTorrent().getUploaded();
						size = seeder.getTorrent().getSize();
					}

					long totalDownload = 0;
					for (Client leecher : leechers)
					{
						totalDownload += leecher.getTorrent().getDownloaded();
					}

					long downloadTime = leechEnd - leechStart;
					double copies = ((double) seederTotalUpload / (double) size);
					printer.writeResults(new TestResult(setup, copies, size, totalDownload, seederTotalUpload, downloadTime));

					closeGui();
				}
				printer.nextTestSetup();
			}
			printer.closeTestRun();
		}
	}

	private void giveGuiStartTime(long leechStart)
	{
		if(showGui)
		{
			testGui.giveStartTime(leechStart);
		}
	}

	private void showGui(ArrayList<Client> leechers, String testName, TestSetup setup)
	{
		if (showGui)
		{
			testGui = new PieceCompletionGui(leechers);

			frame = new JFrame(testName + " - " + setup.clientType + " " + setup.seederUploadRateInKilobyte + "KB/s" + " S:" + setup.seeders + " L:" + setup.leechers);
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.setContentPane(testGui);
			frame.pack();
			frame.setVisible(true);
		}
	}

	private void closeGui()
	{
		if (showGui)
		{
			testGui.closeDown();
			frame.setVisible(false);
			frame.dispose();
			testGui = null;
			frame = null;
		}
	}

	private Client createLeecher(int leechNumber, ClientType clientType, File torrent, ArrayList<Client> seeders)
	{
		File saveDir = createSaveDir("leecher-" + leechNumber);
		deleteDir(saveDir);
		return createClient(saveDir, Integer.MAX_VALUE, clientType, torrent);
	}

	private void deleteDir(File saveDir)
	{
		if (!saveDir.exists())
			return;

		File[] files = saveDir.listFiles();
		if (files != null)
			for (File file : files)
				if (!file.delete())
					throw new RuntimeException("Could not delete file: " + file);

		if (!saveDir.delete())
			throw new RuntimeException("Could not delete dir: " + saveDir);
	}

	private Client startSeeder(int uploadRateLimitInKilobytes, File torrent)
	{
		Client client = createClient(createSaveDir("seeder"), uploadRateLimitInKilobytes, ClientType.Plain, torrent);
		client.share();
		return client;
	}

	private Client createClient(File saveDir, int uploadRateLimitInKilobytes, ClientType clientType, File torrentFile)
	{
		try
		{
			if (!(saveDir.exists() || saveDir.mkdir()))
				throw new RuntimeException("Could not create saveDir: " + saveDir);

			switch (clientType)
			{
				case Plain:
					return new Client(InetAddress.getLocalHost(), SharedTorrent.fromFile(torrentFile, saveDir, uploadRateLimitInKilobytes));

				case LocalPeerDiscovery:
					return new LocalPeerDiscoveryClient(InetAddress.getLocalHost(), SharedTorrent.fromFile(torrentFile, saveDir, uploadRateLimitInKilobytes));

				case CollaboratingClient:
					return new CollaboratingLanClient(InetAddress.getLocalHost(), CollaboratingPieceSelectionTorrent.fromFile(torrentFile, saveDir, uploadRateLimitInKilobytes));

				case LanGiveAway:
					return new LanGiveAwayClient(InetAddress.getLocalHost(), CollaboratingPieceSelectionTorrent.fromFile(torrentFile, saveDir, uploadRateLimitInKilobytes));
			}

			throw new RuntimeException("Did not know about clientType: " + clientType);

		} catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
			throw new RuntimeException(e);
		}
	}

	private File createSaveDir(String appendToSaveDirectory)
	{
		return new File(outputDirectory, appendToSaveDirectory);
	}
}
