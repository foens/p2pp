package olefoens.tests;

import java.io.*;

public class TestResultPrinter
{
	private final File outputDirectory;
	private static final String header = "Leechers, Seeders, WanSpeed (kb/s), Time (s), WanUpload (bytes), FileSize (bytes)\r\n";
	private BufferedWriter output;

	public TestResultPrinter(File outputDirectory)
	{
		this.outputDirectory = outputDirectory;
	}

	public void openTestRun(String testName)
	{
		System.out.println(testName);
		String filename = testName + ".csv";
		File outputFile = new File(outputDirectory, filename);
		try
		{
			output = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));
		} catch (FileNotFoundException e)
		{
			throw new RuntimeException(e);
		}
		write(header);
	}

	public void writeResults(TestResult result)
	{
		write(buildOutputString(result));
	}

	public void nextTestSetup()
	{
		write("\r\n");
	}

	private void write(String string)
	{
		System.out.print(string);
		try
		{
			output.write(string);
			output.flush();
		} catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	private String buildOutputString(TestResult r)
	{
		AppendingLineStringBuilder s = new AppendingLineStringBuilder();

		s.append(r.setup.leechers + ", ");
		s.append(r.setup.seeders + ", ");
		s.append(r.setup.seederUploadRateInKilobyte + ", ");
		s.append(r.downloadTimeInMs/1000 + ", ");
		s.append(r.totalUpload + ", ");
		s.append(r.size + ", ");
		s.appendLine((double)r.totalUpload / (double)r.size  + "");

		return s.toString();
	}

	public void closeTestRun()
	{
		try
		{
			output.close();
		} catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		output = null;
	}

	private static final class AppendingLineStringBuilder
	{
		private final StringBuilder builder = new StringBuilder();

		public void append(String string)
		{
			builder.append(string);
		}

		public void appendLine(String string)
		{
			builder.append(string);
			builder.append("\r\n");
		}

		@Override
		public String toString()
		{
			return builder.toString();
		}
	}
}
