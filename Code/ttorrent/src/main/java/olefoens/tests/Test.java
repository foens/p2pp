package olefoens.tests;

import java.io.File;
import java.util.ArrayList;

public class Test
{
	private static final File testDir = new File(System.getProperty("user.home"), "test");
	private static final File smallTorrent = new File(testDir, "50MB.torrent");
	private static final File largeTorrent = new File(testDir, "500MB.torrent");
	private static final File[] torrents = {smallTorrent, largeTorrent};
	private static final ClientType[] clientTypes = { ClientType.LanGiveAway, ClientType.LocalPeerDiscovery};
	private static final boolean showGui = true;

	public static void main(String[] args) throws InterruptedException
	{
		final int testRuns = 10;

		ArrayList<TestSuite> suites = new ArrayList<TestSuite>();
		addTests(suites, testRuns);

		new TestRunner(testDir, showGui).runTests(suites, new TestResultPrinter(testDir));
	}

	private static void addTests(ArrayList<TestSuite> suites, int testRuns)
	{
		for (File torrent : torrents)
		{
			String torrentName = torrent.getName();
			torrentName = torrentName.substring(0, torrentName.indexOf('.'));
			for (ClientType clientType : clientTypes)
			{
				TestSuite suite = new TestSuite("WanSpeedTest " + torrentName + " " + clientType);
				suite.setups = createWanSpeedTests(testRuns, torrent, clientType);
				suites.add(suite);
			}

			for (ClientType clientType : clientTypes)
			{
				TestSuite suite = new TestSuite("SeederLeecherRatioTest " + torrentName + " " + clientType);
				suite.setups = createSeederLeecherRatioTest(testRuns, torrent, clientType);
				suites.add(suite);
			}
		}
	}

	private static ArrayList<TestSetup> createWanSpeedTests(int testRuns, File torrent, ClientType clientType)
	{
		final int leechers = 14;
		final int seeders = 2;
		ArrayList<TestSetup> setups = new ArrayList<TestSetup>();
		for (int uploadRateInKb = 512; uploadRateInKb <= 4 * 1024; uploadRateInKb *= 2)
		{
			setups.add(new TestSetup(seeders, leechers, uploadRateInKb, torrent, clientType, testRuns));
		}
		return setups;
	}

	private static ArrayList<TestSetup> createSeederLeecherRatioTest(int testRuns, File torrent, ClientType clientType)
	{
		final int uploadRateInKb = 2048;
		ArrayList<TestSetup> setups = new ArrayList<TestSetup>();
		for (int leechers = 2; leechers <= 8; leechers *= 2)
		{
			final int seeders = 1;
			setups.add(new TestSetup(seeders, leechers, uploadRateInKb, torrent, clientType, testRuns));
		}
		for (int seeders = 2; seeders <= 8; seeders *= 2)
		{
			final int leechers = 8;
			setups.add(new TestSetup(seeders, leechers, uploadRateInKb, torrent, clientType, testRuns));
		}
		return setups;
	}
}