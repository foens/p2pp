package olefoens.tests;

import java.util.ArrayList;

public class TestSuite
{
	public final String name;
	public ArrayList<TestSetup> setups = new ArrayList<TestSetup>();

	public TestSuite(String name)
	{
		this.name = name;
	}
}
