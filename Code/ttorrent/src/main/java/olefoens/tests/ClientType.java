package olefoens.tests;

public enum ClientType
{
	LocalPeerDiscovery,
	Plain,
	CollaboratingClient,
	LanGiveAway
}
