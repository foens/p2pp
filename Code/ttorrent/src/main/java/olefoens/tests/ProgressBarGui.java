package olefoens.tests;

import com.turn.ttorrent.client.Client;
import com.turn.ttorrent.client.SharedTorrent;

import javax.swing.*;
import java.util.ArrayList;

public class ProgressBarGui extends JPanel
{
	private final ArrayList<Client> leechers;
	private ArrayList<JProgressBar> progressBars = new ArrayList<JProgressBar>();
	private Thread thread;
	private static final int maximum = 1000000;

	public ProgressBarGui(ArrayList<Client> leechers)
	{
		this.leechers = leechers;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		for (Client ignored : leechers)
		{
			JProgressBar progressBar = new JProgressBar();
			progressBars.add(progressBar);
			progressBar.setMaximum(maximum);
			progressBar.setValue((int) (Math.random() * 100));
			add(progressBar);
		}
		thread = new UpdateThread();
		thread.start();
	}

	public void updateProgress(int leecher, int progress)
	{
		progressBars.get(leecher).setValue(progress);
	}

	public void closeDown()
	{
		thread.interrupt();
	}

	private class UpdateThread extends Thread
	{
		@Override
		public void run()
		{
			try
			{
				while (true)
				{
					Thread.sleep(100);
					for (int i = 0; i < leechers.size(); i++)
					{
						Client leecher = leechers.get(i);
						updateProgress(i, getProgress(leecher));
					}
				}
			} catch (InterruptedException e)
			{
				// ignore
			}
		}

		private int getProgress(Client leecher)
		{
			SharedTorrent torrent = leecher.getTorrent();
			return (int) (torrent.isInitialized()
					? (float) torrent.getCompletedPieces().cardinality() /
					(float) torrent.getPieceCount() * maximum
					: 0.0f);
		}
	}
}