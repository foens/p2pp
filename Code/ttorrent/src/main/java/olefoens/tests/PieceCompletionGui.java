package olefoens.tests;

import com.turn.ttorrent.client.Client;
import com.turn.ttorrent.client.SharedTorrent;
import com.turn.ttorrent.client.peer.SharingPeer;
import olefoens.collaborate.CollaboratingPieceSelectionTorrent;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.concurrent.TimeUnit;

public class PieceCompletionGui extends JPanel
{
	private final ArrayList<Client> leechers;
	private ArrayList<ArrayList<JPanel>> pieces = new ArrayList<ArrayList<JPanel>>();
	private Thread thread;
	private final JLabel piecesLeft = new JLabel();
	private final JLabel connections = new JLabel();
	private final JLabel downloads = new JLabel();
	private final JLabel duplicates = new JLabel();
	private final JLabel runtime = new JLabel();

	private static final Color NO_COLOR_YET = Color.WHITE;
	private static final Color REQUESTED_COLOR = Color.RED;
	private static final Color FETCHED_FROM_WAN_COLOR = Color.BLACK;
	private static final Color FETCHED_FROM_LAN_COLOR = Color.BLUE;
	private static final Color LAN_IS_FETCHING_COLOR = Color.GREEN;
	private static final Color DUPLICATE_COLOR = Color.MAGENTA;
	private final int pieceCount;
	private long leechStart = -1;

	public PieceCompletionGui(ArrayList<Client> leechers)
	{
		this.leechers = leechers;
		JPanel gridPanel = new JPanel();
		pieceCount = leechers.get(0).getTorrent().getPieceCount();
		gridPanel.setLayout(new GridLayout(leechers.size(), pieceCount));
		for (Client ignored : leechers)
		{
			ArrayList<JPanel> panels = new ArrayList<JPanel>();
			pieces.add(panels);
			for (int piece = 0; piece < pieceCount; piece++)
			{
				JPanel panel = new JPanel();
				panels.add(panel);
				gridPanel.add(panel);
			}
		}

		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new GridLayout(0, 2));
		infoPanel.add(new JLabel("Pieces: ", JLabel.RIGHT));
		infoPanel.add(new JLabel(String.valueOf(pieceCount * leechers.size())));

		infoPanel.add(new JLabel("Pieces left: ", JLabel.RIGHT));
		infoPanel.add(piecesLeft);

		infoPanel.add(new JLabel("Connections: ", JLabel.RIGHT));
		infoPanel.add(connections);

		infoPanel.add(new JLabel("Requests: ", JLabel.RIGHT));
		infoPanel.add(downloads);

		infoPanel.add(new JLabel("Seeder copies sent: ", JLabel.RIGHT));
		infoPanel.add(duplicates);

		infoPanel.add(new JLabel("Runtime: ", JLabel.RIGHT));
		infoPanel.add(runtime);

		JPanel legendPanel = new JPanel();
		legendPanel.setLayout(new GridLayout(0, 2));
		addLegend("Nothing happened yet", NO_COLOR_YET, legendPanel);
		addLegend("Piece is being requested", REQUESTED_COLOR, legendPanel);
		addLegend("Piece fetched from WAN", FETCHED_FROM_WAN_COLOR, legendPanel);
		addLegend("Piece fetched from LAN", FETCHED_FROM_LAN_COLOR, legendPanel);
		addLegend("Piece fetched from WAN, but same piece fetched from WAN multiple times", DUPLICATE_COLOR, legendPanel);
		addLegend("Piece has been/is being downloaded from other LAN peer", LAN_IS_FETCHING_COLOR, legendPanel);

		setLayout(new GridLayout(3, 1));
		add(gridPanel);
		add(infoPanel);
		add(legendPanel);

		thread = new UpdateThread();
		thread.start();
	}

	private void addLegend(String name, Color color, JPanel panel)
	{
		JPanel colorPanel = new JPanel();
		colorPanel.setBackground(color);
		panel.add(colorPanel);

		panel.add(new JLabel(name));
	}

	public void closeDown()
	{
		thread.interrupt();
	}

	public void giveStartTime(long leechStart)
	{
		this.leechStart = leechStart;
	}

	private class UpdateThread extends Thread
	{
		@Override
		public void run()
		{
			try
			{
				while (true)
				{
					Thread.sleep(100);
					updateColors();
				}
			} catch (InterruptedException e)
			{
				updateColors();
				SharedTorrent t = leechers.get(0).getTorrent();

				long totalPieces = t.getPieceCount();
				long totalPiecesSize = 0;
				for(int i = 0; i<totalPieces; i++)
				{
					totalPiecesSize += t.getPiece(i).size();
				}
				long piecesFromSeeder = 0;
				long piecesFromSeederSize = 0;
				for (int column = 0; column < t.getPieceCount(); column++)
				{
					for (int client = 0; client < leechers.size(); client++)
					{
						Color bg = pieces.get(client).get(column).getBackground();
						if (bg == FETCHED_FROM_WAN_COLOR || bg == DUPLICATE_COLOR)
						{
							piecesFromSeeder++;
							piecesFromSeederSize += t.getPiece(column).size();
						}
					}
				}

				System.out.println("Total pieces: " + totalPieces + ":" + totalPiecesSize);
				System.out.println("Pieces from seeders: " + piecesFromSeeder + ":" + piecesFromSeederSize);
				System.out.println((double) (piecesFromSeeder) / (double) (totalPieces) + ":" + (double) (piecesFromSeederSize) / (double) (totalPiecesSize));
			}
		}

		private void updateColors()
		{
			long piecesLeft = 0;
			long connections = 0;
			long downloading = 0;
			for (int client = 0; client < leechers.size(); client++)
			{
				Client leecher = leechers.get(client);
				connections += leecher.connected.values().size();
				for(SharingPeer p : leecher.connected.values())
				{
					if(p.isDownloading())
						downloading++;
				}
				SharedTorrent torrent = leecher.getTorrent();
				if (!torrent.isInitialized())
					break;

				BitSet completedPieces = torrent.getCompletedPieces();
				BitSet requestedPieces = torrent.getRequestedPieces();
				BitSet collaboratingPieces = null;
				BitSet completedFromLanPieces = torrent.completedFromLanPieces;
				if (torrent instanceof CollaboratingPieceSelectionTorrent)
				{
					CollaboratingPieceSelectionTorrent collaboratingPieceSelectionTorrent = (CollaboratingPieceSelectionTorrent) torrent;
					collaboratingPieces = collaboratingPieceSelectionTorrent.collaboratorsFetchingPieces;
				}
				for (int piece = 0; piece < torrent.getPieceCount(); piece++)
				{
					Color pieceColor = NO_COLOR_YET;
					if (collaboratingPieces != null && collaboratingPieces.get(piece))
						pieceColor = LAN_IS_FETCHING_COLOR;
					if (requestedPieces.get(piece))
						pieceColor = REQUESTED_COLOR;
					if (completedPieces.get(piece))
						pieceColor = FETCHED_FROM_WAN_COLOR;
					if (completedFromLanPieces.get(piece))
						pieceColor = FETCHED_FROM_LAN_COLOR;

					if(pieceColor == NO_COLOR_YET || pieceColor == LAN_IS_FETCHING_COLOR || pieceColor == REQUESTED_COLOR)
						piecesLeft++;

					JPanel jPanel = pieces.get(client).get(piece);
					if (jPanel.getBackground() != DUPLICATE_COLOR)
						jPanel.setBackground(pieceColor);
				}
			}

			NumberFormat p = DecimalFormat.getPercentInstance();
			p.setMinimumFractionDigits(2);
			p.setMaximumFractionDigits(2);

			PieceCompletionGui.this.piecesLeft.setText(String.valueOf(piecesLeft)+ " = " + p.format((double) (piecesLeft) / (double) (pieceCount * leechers.size())));
			PieceCompletionGui.this.connections.setText(String.valueOf(connections));
			PieceCompletionGui.this.downloads.setText(String.valueOf(downloading));

			long duplicates = 0;
			for (int column = 0; column < leechers.get(0).getTorrent().getPieceCount(); column++)
			{
				int fromSeeders = 0;
				for (int client = 0; client < leechers.size(); client++)
				{
					Color bg = pieces.get(client).get(column).getBackground();
					if (bg == FETCHED_FROM_WAN_COLOR || bg == DUPLICATE_COLOR)
					{
						fromSeeders++;
					}
				}
				duplicates += (fromSeeders);
				if (fromSeeders > 1)
				{

					for (int client = 0; client < leechers.size(); client++)
					{
						JPanel jPanel = pieces.get(client).get(column);
						if (jPanel.getBackground() == FETCHED_FROM_WAN_COLOR)
						{
							jPanel.setBackground(DUPLICATE_COLOR);
						}
					}
				}
			}

			p = NumberFormat.getInstance();
			p.setMaximumFractionDigits(2);
			p.setMinimumFractionDigits(2);

			PieceCompletionGui.this.duplicates.setText(p.format((double) (duplicates) / (double) (pieceCount)));
			long n = System.currentTimeMillis();
			long s = leechStart;
			if(s != -1)
			{
				long millis = n - s;
				String time = String.format("%02d:%02d:%02d",
						TimeUnit.MILLISECONDS.toHours(millis),
						TimeUnit.MILLISECONDS.toMinutes(millis) -
								TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
						TimeUnit.MILLISECONDS.toSeconds(millis) -
								TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
				PieceCompletionGui.this.runtime.setText(time);
			}
		}
	}
}