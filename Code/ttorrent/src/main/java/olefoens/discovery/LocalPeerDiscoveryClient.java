package olefoens.discovery;

import com.turn.ttorrent.client.Client;
import com.turn.ttorrent.client.SharedTorrent;

import java.io.IOException;
import java.net.InetAddress;

public class LocalPeerDiscoveryClient extends Client
{
	private LocalSubnetBroadcaster broadcaster;

	public LocalPeerDiscoveryClient(InetAddress address, SharedTorrent torrent) throws IOException
	{
		super(address, torrent);
		broadcaster = new LocalSubnetBroadcaster(service.getSocketAddress().getPort(), torrent.getHexInfoHash(), this);
		broadcaster.start();
	}

	@Override
	public void stop(boolean wait)
	{
		super.stop(wait);
		broadcaster.interrupt();
	}
}
