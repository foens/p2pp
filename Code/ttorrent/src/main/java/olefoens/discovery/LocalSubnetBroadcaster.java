package olefoens.discovery;

import com.turn.ttorrent.client.Client;
import com.turn.ttorrent.common.Peer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LocalSubnetBroadcaster extends Thread
{
	private static final Logger logger =
			LoggerFactory.getLogger(LocalSubnetBroadcaster.class);

	private final int port;
	private final String hash;
	private final Client client;
	private DatagramSocket socket;
	private static final long STARTUP_PERIOD_SPAMMING = 20000;
	private static final long STARTUP_PERIOD_DELAY_BETWEEN_PACKAGES = 100;
	private static final long NORMAL_PERIOD_DELAY_BETWEEN_PACKAGES = 2000;
	private static final int BROADCAST_PORT = 6771;
	private MulticastSocket receiveSocket;
	private static final String BROADCAST_GROUP = "239.192.152.143";

	public LocalSubnetBroadcaster(int port, String hash, Client client)
	{

		this.port = port;
		this.hash = hash;
		this.client = client;
		try
		{
			this.socket = new DatagramSocket();

			receiveSocket = new MulticastSocket(BROADCAST_PORT);
			receiveSocket.joinGroup(InetAddress.getByName(BROADCAST_GROUP));
			receiveSocket.setLoopbackMode(false);
			receiveSocket.setSoTimeout(1);
		} catch (SocketException e)
		{
			throw new RuntimeException(e);
		} catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	private void broadcast()
	{
		byte[] message = generateBroadcastMessage();
		try
		{
			logger.info("Sending broadcast message");
			socket.send(new DatagramPacket(message, message.length, InetAddress.getByName(BROADCAST_GROUP), BROADCAST_PORT));
		} catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	private byte[] generateBroadcastMessage()
	{
		String message = "BT-SEARCH * HTTP/1.1\r\n" +
				"Host: 239.192.152.143:6771\r\n" +
				"Port: " + port + "\r\n" +
				"Infohash: " + hash + "\r\n" +
				"\r\n\r\n";
		logger.debug("Sending broadcast message: {}", message);
		return message.getBytes();
	}

	private BroadcastInfo parseBroadcastMessage(DatagramPacket packet) throws BadBroadcastMessage
	{
		String message = new String(packet.getData(), 0, packet.getLength());
		logger.debug("Received broadcast message: {}", message);
		Pattern pattern = Pattern.compile("BT-SEARCH.*Port: (?<port>\\d+)\r\nInfohash: (?<hash>.*)\r\n\r\n\r\n", Pattern.DOTALL | Pattern.MULTILINE);
		Matcher matcher = pattern.matcher(message);
		if(matcher.matches())
		{
			int port = Integer.parseInt(matcher.group("port"));
			String infoHash = matcher.group("hash");
			logger.debug("Broadcast message with port {} and hash {}", port, infoHash);

			return new BroadcastInfo(packet.getAddress().getHostAddress(), port);
		} else
		{
			throw new BadBroadcastMessage();
		}
	}

	private void handleReceivedBroadcasts() throws IOException
	{
		try
		{
			byte[] buffer = new byte[1024];
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			receiveSocket.receive(packet);

			BroadcastInfo broadcastInfo = parseBroadcastMessage(packet);

			if(client.getPeerSpec().getAddress().equals(packet.getAddress()) && client.getPeerSpec().getPort() == broadcastInfo.port)
			{
				// Ignore own packets
				return;
			}

			ArrayList<Peer> peers = new ArrayList<Peer>();
			Peer peer = new Peer(broadcastInfo.ip, broadcastInfo.port, true);
			peers.add(peer);
			logger.info("Found peer with broadcast: {}", peer);

			client.handleDiscoveredPeers(peers);

		} catch(SocketTimeoutException e)
		{
			// No data received. Just ignore
		} catch (BadBroadcastMessage badBroadcastMessage)
		{
			// Don't do anything
			logger.error("Invalid broadcast message");
		}
	}

	@Override
	public void run()
	{
		long startTime = System.currentTimeMillis();
		try
		{
			while (true)
			{
				handleReceivedBroadcasts();

				if (startTime + STARTUP_PERIOD_SPAMMING > System.currentTimeMillis())
				{
					broadcast();
					Thread.sleep(STARTUP_PERIOD_DELAY_BETWEEN_PACKAGES);
				} else
				{
					broadcast();
					Thread.sleep(NORMAL_PERIOD_DELAY_BETWEEN_PACKAGES);
				}
			}
		} catch (InterruptedException e)
		{
			//e.printStackTrace();
		} catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	private class BroadcastInfo
	{
		public String ip;
		public int port;

		public BroadcastInfo(String ip, int port)
		{
			this.ip = ip;
			this.port = port;
		}
	}

	private class BadBroadcastMessage extends Exception
	{
	}
}
