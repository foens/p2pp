﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Experiments;
using MonoTorrent.Client;
using MonoTorrent.Common;

namespace GuiExperiments
{
    public partial class MainForm : Form
    {
        private Form otherForm;
        private TableLayoutPanel panel;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            otherForm = new Form();
            
            Torrent torrent =
                Torrent.Load(@"C:\Users\elumineX\Desktop\torrenttest\debian-6.0.7-i386-businesscard-tracker-fixed.iso.torrent");

            int nrOfColumns = torrent.Pieces.Count;
            float colSizePercent = 100f/nrOfColumns;

            panel = new TableLayoutPanel
                {
                    Dock = DockStyle.Fill,
                    Location = new Point(0, 0),
                    ColumnCount = nrOfColumns,
                    RowCount = 1
                };

            panel.RowStyles.Add(new ColumnStyle(SizeType.Percent, 100f));

            for (int i = 0; i < nrOfColumns; i++)
            {
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, colSizePercent));
                Label l = new Label();
                l.BackColor = Color.Green;
                l.Dock = DockStyle.Fill;
                panel.Controls.Add(l, i, 0);
            }
                

            //panel.BorderStyle = BorderStyle.FixedSingle;
            //panel.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;

            otherForm.Controls.Add(panel);
            otherForm.Show();
        }

        private void ButtonStart_Click(object sender, EventArgs e)
        {
  
        }

        private void Experiment_LeecherPieceHashed(object sender, PieceHashedEventArgs pieceHashedEventArgs)
        {
           
        }

        private void RunExperiment()
        {
            /*Experiment experiment = new Experiment(@"C:\Users\elumineX\Desktop\torrenttest", 4, 1, 1024,
                                        @"C:\Users\elumineX\Desktop\torrenttest\debian-7.0.0-powerpc-CD-1-tracker-fixed.iso.torrent", 5000);
            experiment.Completed += OnExperimentCompleted;
            experiment.RunAsync();

            while (!done)
            {
                Console.Clear();
                foreach (var leecher in experiment.Leechers)
                {
                    int nrKnownPeers = leecher.Peers.ActivePeers.Count + leecher.Peers.AvailablePeers.Count +
                                       leecher.Peers.BusyPeers.Count + leecher.Peers.BannedPeers.Count;

                    Console.WriteLine("Leecher: {0:N1}% [Peers: {1}]", leecher.Progress, nrKnownPeers);
                }

                double totalCompletion = experiment.Leechers.Average(leecher => leecher.Progress);
                Console.Write("Total: {0:N1}% complete", totalCompletion);
                Thread.Sleep(1000);
            }

            Console.WriteLine(
                "\nDone. Stats (#L, #S, WanSpeed, sum(wanUL), fileSize, time(s)):\n {0}, {1}, {2} mb/s, {3} mb, {4} mb, {5} s", res.NrLeechers,
                res.NrSeeders, res.WanSpeedKbps / 1024, res.TotalBytesWanUpload / (1024 * 1024), res.FileSizeBytes / (1024 * 1024), res.CompletionTimeS);

            Console.WriteLine("Press enter to close.");
            Console.Read();*/
        }
    }
}
