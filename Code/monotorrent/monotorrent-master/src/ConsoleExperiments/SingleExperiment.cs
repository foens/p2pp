﻿#define DISABLE_DHT

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using MonoTorrent.Client;
using MonoTorrent.Client.Encryption;
using MonoTorrent.Common;

namespace Experiments
{
    public class SingleExperiment
    {
        public event EventHandler<CriticalExceptionEventArgs> PeerException = delegate { }; 
        public event EventHandler<TorrentEventArgs> PortNotFreeException = delegate { }; 
        public event EventHandler<PieceHashedEventArgs> LeecherPieceHashed = delegate { };
        public event EventHandler<ExperimentResults> Completed = delegate { };
        public List<TorrentManager> Seeders { get; private set; }
        public List<TorrentManager> Leechers { get; private set; }
        public double Progress
        {
            get
            {
                return Leechers.Average(leecher => leecher.Progress);
            }
        }

        private string workingDir;
        private int nrLeechers;
        private int nrSeeders;
        private int wanSpeedKbps;
        private string torrentFile;
        private int nrOfRunningLeechers;
        private Thread mainThread;
        private ManualResetEvent terminateThread = new ManualResetEvent(false);
        private ManualResetEvent initialized = new ManualResetEvent(false);
        private DateTime experimentStart, experimentEnd;
        private int nrOfSeedersHashCompleted = 0;

        public SingleExperiment(string workingDir, int nrLeechers, int nrSeeders, int wanSpeedKbps, string torrentFile)
        {
            this.workingDir = workingDir;
            this.nrLeechers = nrLeechers;
            this.nrSeeders = nrSeeders;
            this.wanSpeedKbps = wanSpeedKbps;
            this.torrentFile = torrentFile;
        }

        public void RunAsync()
        {
            if (ExperimentIsRunning())
                throw new InvalidOperationException("Experiment already running");

            mainThread = new Thread(RunInternal);
            mainThread.Start();
            initialized.WaitOne();
        }

        public void WaitForCompletion()
        {
            mainThread.Join();
        }

        public void Stop()
        {
            terminateThread.Set();
            mainThread.Join();
        }

        private bool ExperimentIsRunning()
        {
            return mainThread != null && mainThread.IsAlive;
        }

        private void RunInternal()
        {
            CheckSourceFilesPresence();
            CleanWorkingDir();
            Seeders = new List<TorrentManager>();
            Leechers = new List<TorrentManager>();

            for (int i = 0; i < nrSeeders; i++)
            {
                TorrentManager manager = StartSeeder();
                Seeders.Add(manager);
            }

            WaitForSeederHashingCompletion();

            for (int i = 0; i < nrLeechers; i++)
            {
                TorrentManager manager = StartLeecher();
                Leechers.Add(manager);
            }

            initialized.Set();

            for (int i = 0; i < nrLeechers; i++)
            {
                for (int j = 0; j < nrSeeders; j++)
                {
                    int iCapture = i;
                    int jCapture = j;
                    ClientEngine.MainLoop.Queue(
                        () => Leechers[iCapture].AddPeersCore(
                            new Peer(
                                "",
                                new Uri(
                                    String.Format(
                                        "tcp://{0}:{1}", GetLocalIPAddress(), Seeders[jCapture].Engine.Settings.ListenPort))))
                        );
                }
            }

            WaitForLeecherCompletion();

            ExperimentResults res = GetExprimentResult();
            StopInternal();
            FireCompletedEvent(res);
        }

        private void WaitForSeederHashingCompletion()
        {
            while (nrOfSeedersHashCompleted != Seeders.Count)
            {
                if (terminateThread.WaitOne(0))
                    break;
                else
                    terminateThread.WaitOne(500);
            }
        }

        private string GetLocalIPAddress()
        {
            IPHostEntry host;
            string localIP = "";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                localIP = ip.ToString();

                String[] temp = localIP.Split('.');
                if (ip.AddressFamily == AddressFamily.InterNetwork && temp[0] == "192")
                {
                    break;
                }
                else localIP = null;
            }
            return localIP;
        }

        private ExperimentResults GetExprimentResult()
        {
            int experimentTimeInS = (int)Math.Round((experimentEnd - experimentStart).TotalSeconds);
            long totalBytesUploadedWan = Seeders.Sum(seeder => seeder.Monitor.DataBytesUploaded);
            long torrentSize = Torrent.Load(torrentFile).Size;

            return new ExperimentResults(nrLeechers, nrSeeders, wanSpeedKbps, experimentTimeInS, totalBytesUploadedWan, torrentSize);
        }

        private void CheckSourceFilesPresence()
        {
            Torrent torrent = Torrent.Load(torrentFile);

            if (torrent.Files.Any(file => !File.Exists(Path.Combine(workingDir, file.FullPath))))
            {
                throw new FileNotFoundException("Could not find source file in dir " + workingDir);
            }
        }

        private void CleanWorkingDir()
        {
            Directory.GetDirectories(workingDir).ToList().ForEach(dir => Directory.Delete(dir, true));
        }

        private void Reset()
        {
            CleanWorkingDir();
            terminateThread.Reset();
            initialized.Reset();
            Seeders = null;
            Leechers = null;
            nrOfRunningLeechers = 0;
            nrOfSeedersHashCompleted = 0;
        }

        private void StopInternal()
        {
            foreach (var seeder in Seeders)
            {
                seeder.Stop();
                seeder.Engine.StopAll();
                seeder.Engine.Dispose();
            }

            foreach (var leecher in Leechers)
            {
                leecher.Stop();
                leecher.Engine.StopAll();
                leecher.Engine.Dispose();
            }

            Reset();
        }

        private void WaitForLeecherCompletion()
        {
            experimentStart = DateTime.Now;

            int nrLeechersCompleted = 0;

            foreach (var leecher in Leechers)
            {
                leecher.TorrentStateChanged +=
                    delegate(object sender, TorrentStateChangedEventArgs args)
                    {
                        if (args.NewState == TorrentState.Seeding)
                        {
                            nrLeechersCompleted++;
                        }
                    };
            }

            while (nrLeechersCompleted != Leechers.Count)
            {
                if (terminateThread.WaitOne(0))
                    break;
                else
                    terminateThread.WaitOne(500);
            }

            experimentEnd = DateTime.Now;
        }

        private TorrentManager StartSeeder()
        {
            var client = CreateClient(workingDir, wanSpeedKbps);
            client.Engine.LocalPeerSearchEnabled = false;

            client.TorrentStateChanged +=
                (sender, args) =>
                    {
                        if (args.NewState == TorrentState.Seeding)
                            nrOfSeedersHashCompleted++;
                    };

            client.Engine.Listener.StatusChanged +=
                (sender, args) =>
                    {
                        if (client.Engine.Listener.Status == ListenerStatus.PortNotFree)
                            PortNotFreeException(sender, new TorrentEventArgs(client));
                    };

            client.Start();

            return client;
        }

        private TorrentManager StartLeecher()
        {
            string clientWorkingDir = Path.Combine(workingDir, "Leecher" + nrOfRunningLeechers+1);
            var client = CreateClient(clientWorkingDir, 0);

            client.PieceHashed += LeecherPieceHashed;

            client.Start();
            nrOfRunningLeechers++;

            return client;
        }

        private TorrentManager CreateClient(string clientWorkingDir, int ulSpeedLimit)
        {
            Directory.CreateDirectory(clientWorkingDir);

            int port = PortManager.GetNextFreePort();
            var engineSettings = new EngineSettings(clientWorkingDir, port)
            {
                PreferEncryption = false,
                AllowedEncryption = EncryptionTypes.All,
                GlobalMaxUploadSpeed = ulSpeedLimit * 1024
            };

            var engine = new ClientEngine(engineSettings);

            Torrent torrent = Torrent.Load(torrentFile);
            var torrentSettings = new TorrentSettings(4, 150, 0, 0);
            var manager = new TorrentManager(torrent, clientWorkingDir, torrentSettings);
            manager.Settings.EnablePeerExchange = false;

            engine.Register(manager);
 
            return manager;
        }

        private void FireCompletedEvent(ExperimentResults res)
        {
            Completed(this, res);
        }
    }
}
