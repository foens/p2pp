﻿namespace Experiments
{
    class PortManager
    {
        private static int nextFreePort = 5000;

        public static int GetNextFreePort()
        {
            return nextFreePort++;
        }
    }
}
