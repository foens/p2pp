﻿#define DISABLE_DHT

using System;
using System.IO;
using System.Threading;
using MonoTorrent.Client;

namespace Experiments
{
    class Program
    {
        private static string workingDir = @"D:\torrenttest";
        private static string smallTorrentFile = Path.Combine(workingDir, "50MB-trackerless.torrent");
        private static string largeTorrentFile = Path.Combine(workingDir, "500MB-trackerless.torrent");
        private const int ExperimentRepititionCount = 10;

        private static bool done;
        private static SingleExperiment experiment;
        private static ExperimentResults res;

        static void Main(string[] args)
        {
            var experiment = new SingleExperiment(workingDir, 2, 1, 512*2*5, smallTorrentFile);
            experiment.PeerException += (sender, e) => { throw e.Exception; };
            experiment.PortNotFreeException += (sender, e) => { throw new Exception("Port not free: " + e.TorrentManager.Engine.PeerId); };

            var writer = new StreamWriter(Path.Combine(workingDir, "test.txt"));
            
            RunAndLogRepeatedExperiment(experiment, writer, 10);
            writer.Close();

            /*
            Console.CancelKeyPress += delegate { StopExperiment(); };
            
            
            experiment.Completed += OnExperimentCompleted;
            experiment.RunAsync();

            while (!done)
            {
                Console.Clear();
                foreach (var leecher in experiment.Leechers)
                {
                    int nrKnownPeers = leecher.Peers.ActivePeers.Count + leecher.Peers.AvailablePeers.Count +
                                       leecher.Peers.BusyPeers.Count + leecher.Peers.BannedPeers.Count;

                    Console.WriteLine("Leecher: {0:N1}% [Peers: {1}]", leecher.Progress, nrKnownPeers);
                }

                double totalCompletion = experiment.Leechers.Average(leecher => leecher.Progress);
                Console.Write("Total: {0:N1}% complete", totalCompletion);
                Thread.Sleep(1000);
            }

            Console.WriteLine(
                "\nDone. Stats (#L, #S, WanSpeed, sum(wanUL), fileSize, time(s)):\n {0}, {1}, {2} mb/s, {3} mb, {4} mb, {5} s",
                res.NrLeechers,
                res.NrSeeders, res.WanSpeedKbps/1024, res.TotalBytesWanUpload/(1024*1024), res.FileSizeBytes/(1024*1024),
                res.CompletionTimeS);

            Console.WriteLine("Press enter to close.");
            Console.Read();*/
        }

        private static void RunExperimentWanSpeed(bool smallFile)
        {
            string fileName = smallFile ? "wanspeed-Small.txt" : "wanspeed-Large.txt";
            string logFile = Path.Combine(workingDir, fileName);
            string torrentFile = smallFile ? smallTorrentFile : largeTorrentFile;

            using (var writer = new StreamWriter(logFile))
            {
                writer.WriteLine("Leechers, Seeders, WanSpeed (kb/s), Time (s), WanUpload (bytes), FileSize (bytes), WanUL/FileSize");

                RunAndLogRepeatedExperiment(new SingleExperiment(workingDir, 9, 2, 512, torrentFile), writer, ExperimentRepititionCount);
                RunAndLogRepeatedExperiment(new SingleExperiment(workingDir, 9, 2, 512*2, torrentFile), writer, ExperimentRepititionCount);
                RunAndLogRepeatedExperiment(new SingleExperiment(workingDir, 9, 2, 512*4, torrentFile), writer, ExperimentRepititionCount);
                RunAndLogRepeatedExperiment(new SingleExperiment(workingDir, 9, 2, 512*8, torrentFile), writer, ExperimentRepititionCount);
                
                writer.WriteLine("");
            }
        }

        private static void RunAndLogRepeatedExperiment(SingleExperiment experiment, StreamWriter writer, int repeatCount)
        {
            for (int i = 0; i < repeatCount; i++)
            {
                int cursorLeft = Console.CursorLeft;
                int cursorTop = Console.CursorTop;
                var res = RunAndLogExperiment(experiment, writer);
                Console.SetCursorPosition(cursorLeft, cursorTop);
                Console.WriteLine("{0} out of {1} runs completed {2}", i + 1, repeatCount, res);
                Thread.Sleep(1000);
            }   
        }

        private static ExperimentResults RunAndLogExperiment(SingleExperiment experiment, StreamWriter writer)
        {
            bool finished = false;
            ExperimentResults result = null;

            experiment.Completed +=
                (sender, res) =>
                    {
                        finished = true;
                        result = res;
                    };

            experiment.RunAsync();

            while (!finished)
            {
                int cursorLeft = Console.CursorLeft;
                int cursorTop = Console.CursorTop;
                Console.WriteLine("Progress: {0:N1}%  ", experiment.Progress);
                Console.SetCursorPosition(cursorLeft, cursorTop);
                Thread.Sleep(1000);
            }

            writer.WriteLine("{0},{1},{2},{3},{4},{5}",
                             result.NrLeechers, result.NrSeeders, result.WanSpeedKbps, result.CompletionTimeS,
                             result.TotalBytesWanUpload, result.FileSizeBytes);

            return result;
        }

        private static void StopExperiment()
        {
            Console.WriteLine("\nStopping all activity and cleaning up...");
            experiment.Stop();
            Console.Read();
        }

        private static void OnExperimentCompleted(object sender, ExperimentResults res)
        {
            Program.res = res;
            done = true;
        }
    }
}
