﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Experiments
{
    public class ExperimentResults
    {
        public int NrLeechers { get; private set; }
        public int NrSeeders { get; private set; }
        public int WanSpeedKbps { get; private set; }
        public int CompletionTimeS { get; private set; }
        public long TotalBytesWanUpload { get; private set; }
        public long FileSizeBytes { get; private set; }

        public ExperimentResults(int nrLeechers, int nrSeeders, int wanSpeedKbs, int completionTimeS, long totalBytesWanUpload, long fileSizeBytes)
        {
            NrLeechers = nrLeechers;
            NrSeeders = nrSeeders;
            WanSpeedKbps = wanSpeedKbs;
            CompletionTimeS = completionTimeS;
            TotalBytesWanUpload = totalBytesWanUpload;
            FileSizeBytes = fileSizeBytes;
        }

        public override string ToString()
        {
            return
                string.Format(
                    "[{0}, {1}, {2}, {3}, {4}, {5}]",
                    NrLeechers, NrSeeders, CompletionTimeS, WanSpeedKbps, TotalBytesWanUpload, FileSizeBytes);
        }
    }
}
