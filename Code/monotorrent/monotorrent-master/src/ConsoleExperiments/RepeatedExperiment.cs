﻿using System;
using System.Collections.Generic;
using System.Linq;
using MonoTorrent.Client;

namespace Experiments
{
    class RepeatedExperiment
    {
        public event EventHandler<List<ExperimentResults>> Completed;
        public SingleExperiment TargetExperiment { get; private set; }
        private readonly int repetitionCount;
        private readonly List<ExperimentResults> experimentsResults = new List<ExperimentResults>();
 
        public RepeatedExperiment(SingleExperiment target, int repetitionCount)
        {
            this.TargetExperiment = target;
            this.repetitionCount = repetitionCount;

            target.Completed += Experiment_Completed;
        }

        private void Experiment_Completed(object sender, ExperimentResults experimentResults)
        {
            experimentsResults.Add(experimentResults);
        }

        public void Run()
        {
            for (int i = 0; i < repetitionCount; i++)
            {
                TargetExperiment.RunAsync();
                TargetExperiment.WaitForCompletion();
            }

            Completed(this, experimentsResults);
        }

        public void Stop()
        {
            TargetExperiment.Stop();
        }
    }
}
