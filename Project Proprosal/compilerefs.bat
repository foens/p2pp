@ECHO OFF
move Rapport.pdf r.pdf
IF ERRORLEVEL 1 (
  echo msgbox"PDF in use. Fix plz!">msgbox.vbs&msgbox.vbs
  del msgbox.vbs
) else (
  del r.pdf
  pdflatex Rapport.tex
  bibtex Rapport.aux
  pdflatex Rapport.tex
  pdflatex Rapport.tex
)