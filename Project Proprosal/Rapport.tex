\documentclass[a4paper,10pt]{article}
\usepackage[margin=4cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{url}
\usepackage{palatino}
\usepackage{times}
\usepackage{rotating}
\usepackage{multirow}
\usepackage{listings}
\usepackage{color}
\usepackage{textcomp}
\usepackage{amsmath}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[colorlinks,linkcolor=black,citecolor=black,urlcolor=black,linktoc=all]{hyperref}


\usepackage{chronology}


\lstdefinelanguage{JavaScript}{
  keywords={typeof, new, true, false, catch, function, return, null, catch, switch, var, if, in, while, do, else, case, break},
  keywordstyle=\color{blue}\bfseries,
  ndkeywords={class, export, boolean, throw, implements, import, this},
  ndkeywordstyle=\color{darkgray}\bfseries,
  identifierstyle=\color{black},
  sensitive=false,
  comment=[l]{//},
  morecomment=[s]{/*}{*/},
  commentstyle=\color{purple}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  morestring=[b]',
  morestring=[b]"
}

\begin{document}

\title{Project Proposal\\Local Area Network Optimizing BitTorrent Client}

\author{
  Ole \& Kasper\\
  Department of Computer Science, Aarhus University\\
  Aabogade 34, 8200 Aarhus N, Denmark\\
  \makeatletter
  \texttt{20083899, 20083881}\\
  \texttt{\{olera,foens\}@cs.au.dk}
}

\maketitle

\tableofcontents
\newpage
\section{Use cases}
We have two different use cases:
\subsection{Distribution of software updates in large companies}
Suppose you have a company with thousands of computers, each wanting to download a large software update to an installed program. Each computer could fetch the update from an internet update-server, but this has two problems: it's slow and it's ineffective because the same update is downloaded thousands of times on the same connection (WAN). The result is that a lot of bandwidth is wasted on downloading identical files - bandwidth that could potentially have been used for other more productive things in the company. To circumvent this, one could use a P2P system to both minimize internet usage and collectively download the software update and distribute it locally using the higher speed local area network. 

\subsection{Simultaneous download of identical files at LAN parties}
The ultimate goal of a LAN party is to provide a low latency environment for (mostly) playing games. Though players may compete against each other internally it is not uncommon to play with people outside the local network. In addition more of todays games require some form of connectivity to the Internet even to play the games locally \cite{website:alwaysondrm}. This poses strict requirements for the shared WAN connection; it must provide low latency and be available. Due to the goal of LAN parties this is no easy task. At any point in time game developers may release a patch requiring all computers running that game to update. This causes a huge surge in WAN traffic as hundreds of PC's will be requesting the same file from a (usually single) source on the internet. This is very bad for latency and availability and may in the worst case prevent people from even starting their games or be thrown out of servers due to high ping times. 

To solve this problem a P2P system could be utilized for downloading game updates and other large files. Blizzard is an example of a company that already utilizes P2P technology to distribute game updates \cite{website:blizzardp2p}. The goal, however, is not to minimize WAN and maximize LAN traffic but merely to decrease the load on the patch servers. 

What is needed to support this use case is a P2P system that would allow LAN clients to corporate around downloading the files in such a way that it would maximize the usage of the LAN links while minimizing the use of the WAN link.

\section{Idea}
BitTorrent is an excellent tool for distributing files among many peers. A software update could be distributed using BitTorrent and patches and games could also be packaged as such. Many BitTorrent clients ($\mu$Torrent\cite{website:utorrent-lpd}, libtorrent\cite{website:libtorrent-lpd}, MonoTorrent\cite{website:monotorrent-lpd}, \ldots) implement \textit{Local Peer Discovery}, which is a broadcast based mechanism for discovering peers on the local network\cite{website:wiki-lpd}. However, LPD only discovers local peers and prioritizes them over WAN peers. The result is a mechanism that works well for optimizing speed but not for minimizing WAN usage. 

We propose an addition to the BitTorrent protocol that will minimize WAN traffic and maximize LAN traffic, by intelligently choosing peers to download from and upload to based on if they are on the same local area network or not. Compared to LPD our proposal should both be able to: 1) achieve better speeds and 2) minimize possibilities of choking the WAN connection. 1) is achieved by minimizing the amount of duplicate data downloaded from WAN. In other words our client will potentially download the relevant pieces faster. 2) is achieved by the same mechanism but could potentially be even further improved with an optimizing piece selection strategy mentioned in section \ref{sec:experiments}.

\section{Architecture}
\label{sec:arch}
We are going to change a bit in the normal BitTorrent architecture. Each BitTorrent implementation will keep record of peers, just like in the reference implementation, however, we will record if a peer is local or remote. A peer is considered local if it can be discovered using LPD. Our key optimization is to create a local peer swarm which collaborates on getting a BitTorrent file copied from the WAN to the LAN. We will implement a new message, \textit{will-fetch-piece $\#n$}, which will be sent to each local peer when a peer is going to fetch a piece from the WAN side. This will allow other local peers to prioritize fetching other pieces from WAN which are missing on the LAN. An overview can be seen in Figure~\ref{fig:architecture}.

We plan on implementing our architecture by reusing an existent BitTorrent library/framework. If for some reason this turns out to be unfeasible we may write our own (basic) implementation of a BitTorrent client.

\section{Experiments}
\label{sec:experiments}
Our ultimate goal is to make a BitTorrent client that is able to download and distribute files faster than existing BitTorrent implementations when LAN links are present. 

The basic way we accomplish this is described above but there are many concrete optimizations that may  be interesting to experiment with. For example an advanced piece selection strategy could be to estimate the WAN bandwidth of a local peer swarm (ala. BitTyrant \cite{piatek2007incentives}) by keeping track of which pieces are completed at what times in the local swarm. This information may be used to collectively optimize the ratio of WAN to LAN downloads such that the WAN speed is utilized as much as possible without choking it for other users.

Apart from the simple optimization described in section \ref{sec:arch} we will be experimenting with more advanced piece selection strategies and compare the performance of our client to some of the most popular ones like \cite{website:utorrent} and \cite{website:vuze}.

\begin{figure}
	\includegraphics[scale=0.7]{fig/architecture.png}
	\caption{A sketch of the modified BitTorrent world seen from one peer instance}
	\label{fig:architecture}
\end{figure}

\section{Schedule}

\begin{figure}
	\begin{chronology}[1]{0}{7}{370pt}{300pt}
		\event[0]{1}{\textbf{Week 1: }Project proposal}
		\event[1]{2}{\textbf{Week 2: }Research frameworks and related work}
		\event[2]{3}{\textbf{Week 3: }Discovering local peers}
		\event[3]{5}{\textbf{Week 4-5: }Piece priority}
		\event[5]{7}{\textbf{Week 6-7: }Show \& Tell and report writing}
	\end{chronology}
	\caption{Per week overview of our schedule}
	\label{fig:schedule}
\end{figure}

Figure~\ref{fig:schedule} shows an overview of our schedule. Each task is described further below.

\begin{itemize}
  \item \emph{Week 1 - Project proposal}
  \begin{itemize}
      \item Writing this proposal text
  \end{itemize}
      
  \item \emph{Week 2 - Research existing frameworks and related work}
  \begin{itemize}
      \item Find a suitable Java BitTorrent library where we can hook into it. Get it to work. If such does not exist, re-evaluate this plan
      \item Find related work and skim them
  \end{itemize}
      
  \item \emph{Week 3 - Discovering local peers}
  \begin{itemize}
      \item Implement broadcasting on the LAN to discover local peers
      \item Prioritize these local peers
  \end{itemize}
      
  \item \emph{Week 4-5 - Piece priority}
  \begin{itemize}
      \item Insert instrumentation code to measure key values
      \item Implement new method for telling local peers what pieces are being downloaded from the internet
      \item Prioritize fetching pieces from WAN which are not found on LAN
      \item Experiment with different piece selection strategies
  \end{itemize}
      
  \item \emph{Week 6-7 - Show \& Tell and report writing}
  \begin{itemize}
      \item Show and tell about our experiment for the class
      \item Finish up the report and submit it
  \end{itemize}
\end{itemize}

\bibliographystyle{IEEEtran}
\bibliography{references}
\end{document}